package storage;

import java.util.ArrayList;

import model.Klip;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;
import model.Regning;
import model.Rundvisning;
import model.Salg;

public class Storage
{
    private static Storage instance;
    private ArrayList<Salg> alleSalg = new ArrayList<>();
    private ArrayList<Produkt> alleProdukter = new ArrayList<>();
    private ArrayList<Prisliste> allePrislister = new ArrayList<>();
    private ArrayList<Produktgruppe> alleProduktgrupper = new ArrayList<>();
    private ArrayList<Regning> alleRegninger = new ArrayList<>();
    private ArrayList<Salg> alleUdlejninger = new ArrayList<>();
    private ArrayList<Klip> alleKlip = new ArrayList<>();
    private ArrayList<Rundvisning> alleRundvisninger = new ArrayList<>();

    private Storage()
    {

    }

    /**
     * metoden der soerger for storage er Singleton pattern
     *
     * @return storage klassen
     */

    public static Storage getInstance()
    {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }

    public void tilføjSalg(Salg s)
    {
        alleSalg.add(s);
    }

    /**
     * fjerner et salg fra salg listen
     *
     * @param salg
     *            salget der skal fjernes
     */
    public void fjernSalg(Salg salg)
    {
        if (alleSalg.contains(salg)) {
            alleSalg.remove(salg);
        }
    }

    public ArrayList<Salg> getAlleSalg()
    {
        return alleSalg;
    }

    public void tilføjProdukt(Produkt produkt)
    {
        alleProdukter.add(produkt);
    }

    public ArrayList<Produkt> getAlleProdukter()
    {
        return alleProdukter;
    }

    public void setAlleProdukter(ArrayList<Produkt> alleProdukter)
    {
        this.alleProdukter = alleProdukter;
    }

    public ArrayList<Prisliste> getAllePrislister()
    {
        return allePrislister;
    }

    public ArrayList<Produktgruppe> getAlleProduktgrupper()
    {
        return alleProduktgrupper;
    }

    public void setAlleProduktgrupper(ArrayList<Produktgruppe> alleProduktgrupper)
    {
        this.alleProduktgrupper = alleProduktgrupper;
    }

    public void tilføjRegning(Regning regning)
    {
        alleRegninger.add(regning);
    }

    public ArrayList<Regning> getAlleRegninger()
    {
        return alleRegninger;
    }

    public ArrayList<Salg> getAlleUdlejninger()
    {
        return alleUdlejninger;
    }

    public void setAlleUdlejninger(ArrayList<Salg> alleUdlejninger)
    {
        this.alleUdlejninger = alleUdlejninger;
    }

    public ArrayList<Klip> getAlleKlip()
    {
        return alleKlip;
    }

    public void setAlleKlip(ArrayList<Klip> alleKlip)
    {
        this.alleKlip = alleKlip;
    }

    public ArrayList<Rundvisning> getAlleRundvisninger()
    {
        return alleRundvisninger;
    }

    public void setAlleRundvisninger(ArrayList<Rundvisning> alleRundvisninger)
    {
        this.alleRundvisninger = alleRundvisninger;
    }
}