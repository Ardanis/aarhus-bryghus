package model;

public class Antal
{

    private int antal;
    private Pris pris;
    private Salg salg;

    public Antal(int antal, Pris pris)
    {
        this.pris = pris;
        this.antal = antal;
    }

    /**
     * Denne metode bruges til at udregne den samlede pris både med og uden pant
     *
     * @return Den samlede pris returneres.
     */
    public double samletPrisUdregning()
    {
        if (pris.getProdukt().getPant() == 0) {
            return pris.getPris() * antal;
        } else {
            return (pris.getPris() + pris.getProdukt().getPant()) * antal;
        }
    }

    public int getAntal()
    {
        return antal;
    }

    public Pris getPris()
    {
        return pris;
    }

    public Salg getSalg()
    {
        return salg;
    }

    public void setSalg(Salg salg)
    {
        this.salg = salg;
    }

    public void setAntal(int antal)
    {
        this.antal = antal;
    }

    @Override
    public String toString()
    {
        return pris.getProdukt().getNavn() + " " + antal + " Stk. " + (pris.getPris() * antal)
                + "kr.";
    }
}