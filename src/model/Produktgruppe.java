package model;

import java.util.ArrayList;

public class Produktgruppe
{
    private String navn;
    private ArrayList<Produkt> produkter = new ArrayList<>();

    public Produktgruppe(String navn)
    {
        this.navn = navn;
    }

    public String getNavn()
    {
        return navn;
    }

    public void setnavn(String navn)
    {
        this.navn = navn;
    }

    /**
     * tilfoejer et produkt til gruppen og tilføjer produktets gruppe til denne
     * gruppe
     *
     * @param Produkt
     *            produktet der skal tilføjes til produktgruppen
     */

    public void tilføjProdukt(Produkt p)
    {
        produkter.add(p);
        p.setGruppe(this);
    }

    public void fjernProdukt(Produkt p)
    {
        produkter.remove(p);
        p.setGruppe(null);
    }

    public ArrayList<Produkt> getProdukter()
    {
        return produkter;
    }

    @Override
    public String toString()
    {
        return navn;
    }
}