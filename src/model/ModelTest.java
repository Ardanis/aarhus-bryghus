package model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

public class ModelTest {
	
	private Antal antal, antal2; 
	private Pris pris, pris2;
	private Produkt produkt;
	private Produktgruppe pGruppe;
	private Prisliste pListe;
	private Salg salg;
	private Rundvisning rundvisning;
	private Fustage fustage;
	
	@Before
	public void initiate(){
		pGruppe = new Produktgruppe("TestGruppe");
		produkt = new Produkt("Øl", "Det er en Øl", pGruppe);
		pListe = new Prisliste("TestListe");
		pris = new Pris(10, produkt, pListe);
		antal = new Antal(5, pris);
		salg = new Salg(LocalDate.now(), antal);
		rundvisning = new Rundvisning("Øl Rundvisning", "Der er Øl", pGruppe, LocalDateTime.now(), 50, 3);
		fustage = new Fustage("Masse Øl", "MEGET ØL", pGruppe, 25);
		pris2 = new Pris(700, fustage, pListe);
		fustage.addPris(pris2);
		
		
	}
	
	//Antal Testen---------------------------------------------------------

	@Test
	public void prisUdregningTest() {
		assertEquals(50,antal.samletPrisUdregning(), 0.001);
	}
	
	@Test
	public void testGetAntal(){
		assertEquals(5, antal.getAntal());
	}
	
	@Test
	public void getPrisTest(){
		assertEquals(10, antal.getPris().getPris(), 0.001);
	}
	
	@Test
	public void setAntalTest(){
		antal.setAntal(100);
		assertEquals(1000, antal.samletPrisUdregning(),0.001);
	}
	
	//Test af ikke-trivielle metoder--------------------------------------------
	
	@Test
	public void rundvisningGetPrisTest(){
		if(LocalDateTime.now().getHour() >= 16){
			assertEquals(7320, rundvisning.getPris());
		}
		else{
			assertEquals(4970, rundvisning.getPris());
		}
	}
	
	@Test
	public void fustagePrisJusteringTest(){
		assertEquals(840, fustage.prisJustering(30), 0.001);
	}
	
}
