package model;

import java.util.ArrayList;

public class Regning implements Betalingsform
{
    private String kundeNavn;
    private ArrayList<Salg> salg = new ArrayList<>();
    private double samletPris;

    public Regning(String kundeNavn)
    {
        this.kundeNavn = kundeNavn;
    }

    public void tilføjSalg(Salg s)
    {
        salg.add(s);
        samletPris += s.getSamletPris();
    }

    public void betalRegning()
    {
        salg = new ArrayList<>();
        samletPris = 0;
    }

    public void printSalg()
    {
        for (Salg s : salg) {
            System.out.println(s.toString());
        }
    }

    public String getKundeNavn()
    {
        return kundeNavn;
    }

    public double getSamletPris()
    {
        return samletPris;
    }

    public ArrayList<Salg> getTilknyttedeSalg()
    {
        return salg;
    }

    @Override
    public String toString()
    {
        return kundeNavn;
    }
}