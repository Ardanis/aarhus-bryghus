package model;

import java.util.ArrayList;

public class Prisliste
{
    private String navn;
    private ArrayList<Pris> priser = new ArrayList<>();

    public Prisliste(String navn)
    {
        this.navn = navn;
    }

    public String getNavn()
    {
        return navn;
    }

    public void setNavn(String navn)
    {
        this.navn = navn;
    }

    public void tilføjPris(Pris p)
    {
        if (priser == null) {
            priser = new ArrayList<>();
        }
        priser.add(p);
    }

    public ArrayList<Produkt> getProdukter()
    {
        ArrayList<Produkt> produkter = new ArrayList<>();
        for (int i = 0; i < priser.size(); i++) {
            System.out.println(priser.get(i).getProdukt());
            produkter.add(priser.get(i).getProdukt());
        }
        return produkter;
    }

    public ArrayList<Pris> getPriser()
    {
        return priser;
    }

    public void fjernPris(Pris p)
    {
        priser.remove(p);
    }

    @Override
    public String toString()
    {
        return navn;
    }
}