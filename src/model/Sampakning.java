package model;

import java.util.ArrayList;

public class Sampakning extends Produkt

{
    private ArrayList<Produkt> tilknyttedeProdukter = new ArrayList<>();

    public Sampakning(String navn, String beskrivelse, Produktgruppe gruppe)
    {
        super(navn, beskrivelse, gruppe);

    }

    public void tilføjProdukt(Produkt p)
    {
        tilknyttedeProdukter.add(p);
    }

    public void fjernProdukt(Produkt p)
    {
        tilknyttedeProdukter.remove(p);
    }
}