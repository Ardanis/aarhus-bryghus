package model;

import java.time.LocalDateTime;

public class Rundvisning extends Produkt
{
    private LocalDateTime tidspunkt;
    private int gruppeMængde;
    private int studerende;
    private boolean isBetalt = false;

    public Rundvisning(String navn, String beskrivelse, Produktgruppe gruppe,
            LocalDateTime tidspunkt, int gruppeMængde, int studerende)
    {
        super(navn, beskrivelse, gruppe);
        this.tidspunkt = tidspunkt;
        this.gruppeMængde = gruppeMængde;
        this.studerende = studerende;
    }

    public int getPris()
    {
        int i = 100 * gruppeMængde;
        if (tidspunkt.getHour() >= 16) {
            i += (gruppeMængde - studerende) * 50;
        }
        i = i - studerende * 10;
        return i;
    }

    public LocalDateTime getTidspunkt()
    {
        return tidspunkt;
    }

    public int getGruppeMængde()
    {
        return gruppeMængde;
    }

    @Override
    public boolean isRundvisning()
    {
        return true;
    }

    public boolean isBetalt()
    {
        return isBetalt;
    }

    public void setBetalt(boolean value)
    {
        isBetalt = value;
    }

    public int getStuderende()
    {
        return studerende;
    }
}