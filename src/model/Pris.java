package model;

public class Pris
{
    private double pris;
    private Produkt produkt;
    private Prisliste prisliste;

    public Pris(double pris, Produkt produkt, Prisliste prisliste)
    {
        this.pris = pris;
        this.produkt = produkt;
        this.prisliste = (prisliste);
    }

    public double getPris()
    {
        return pris;
    }

    public Produkt getProdukt()
    {
        return produkt;
    }

    public void setPrisliste(Prisliste prisliste)
    {
        this.prisliste = prisliste;
    }

    public Prisliste getPrisliste()
    {
        return prisliste;
    }

    @Override
    public String toString()
    {
        return Double.toString(pris);
    }
}