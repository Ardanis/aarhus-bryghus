package model;

import java.time.LocalDate;

public class Klip implements Betalingsform
{
    private LocalDate dato;
    private Produkt produkt;

    public Klip(LocalDate dato, Produkt produkt)
    {
        super();
        this.dato = dato;
        this.produkt = produkt;
    }

    public LocalDate getDato()
    {
        return dato;
    }

    public void setDato(LocalDate dato)
    {
        this.dato = dato;
    }

    public Produkt getProdukt()
    {
        return produkt;
    }

    public void setProdukt(Produkt produkt)
    {
        this.produkt = produkt;
    }
}