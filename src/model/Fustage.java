package model;

public class Fustage extends Produkt
{
    private int volumen;
    private int pant;

    public Fustage(String navn, String beskrivelse, Produktgruppe gruppe, int volume)
    {
        super(navn, beskrivelse, gruppe);
        this.volumen = volume;
        pant = 1000;
    }

    public double prisJustering(int nyVolumen)
    {
        return this.getPris(0).getPris() / volumen * nyVolumen;
    }

    public int getVolumen()
    {
        return volumen;
    }

    public void setVolumen(int volumen)
    {
        this.volumen = volumen;
    }

    @Override
    public int getPant()
    {
        return pant;
    }
}