package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Salg
{
    private LocalDate salgsDato;
    private double samletPris = 0;
    private ArrayList<Antal> priser = new ArrayList<>();
    private double byttepenge;
    private double manglerAtBetale;
    private boolean isBetalt;

    public Salg(LocalDate salgsDato, Antal pris)
    {
        this.salgsDato = salgsDato;
        this.tilføjPris(pris);
    }

    public double getSamletPris()
    {
        return samletPris;
    }

    public void setSamletPris(double pris)
    {
        this.samletPris = pris;
    }

    /**
     * @param Antal
     *            et Antal af priser Foelgende metode tilf�jer en pris til
     *            arrayet og opdateret den samlede pris
     */
    public void tilføjPris(Antal a)
    {
        priser.add(a);
        samletPris += a.samletPrisUdregning();
    }

    /**
     * @param Antal
     *            Et Antal af priser der allerede er i salget. F�lgende metode
     *            fjerner en pris til arrayet og opdateret den samlede pris
     */
    public void fjernPris(Antal a)
    {
        if (priser.contains(a)) {
            priser.remove(a);
            samletPris -= a.samletPrisUdregning();
        }
    }

    public LocalDate getSalgsDato()
    {
        return salgsDato;
    }

    public ArrayList<Antal> getPriser()
    {
        return priser;
    }

    @Override
    public String toString()
    {
        return "Dato: " + salgsDato.toString() + " Pris: " + samletPris;
    }

    public double getByttepenge()
    {
        return byttepenge;
    }

    public void setByttepenge(double byttepenge)
    {
        this.byttepenge = byttepenge;
    }

    public double getManglerAtBetale()
    {
        return manglerAtBetale;
    }

    public void setManglerAtBetale(double manglerAtBetale)
    {
        this.manglerAtBetale = manglerAtBetale;
    }

    public boolean isBetalt()
    {
        return isBetalt;
    }

    public void setBetalt(boolean isBetalt)
    {
        this.isBetalt = isBetalt;
    }
}