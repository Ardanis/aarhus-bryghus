package model;

import java.util.ArrayList;

public class Produkt
{
    private String navn;
    private String beskrivelse;
    private Produktgruppe gruppe;
    private ArrayList<Pris> tilknyttedePriser = new ArrayList<>();
    private int pant = 0;

    public Produkt(String navn, String beskrivelse, Produktgruppe gruppe)
    {
        this.navn = navn;
        this.beskrivelse = beskrivelse;
        this.gruppe = gruppe;
    }

    public String getNavn()
    {
        return navn;
    }

    public void setNavn(String navn)
    {
        this.navn = navn;
    }

    public String getBeskrivelse()
    {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse)
    {
        this.beskrivelse = beskrivelse;
    }

    public Produktgruppe getGruppe()
    {
        return gruppe;
    }

    public void setGruppe(Produktgruppe gruppe)
    {
        this.gruppe = gruppe;
    }

    public ArrayList<Pris> getTilknyttedePriser()
    {
        return tilknyttedePriser;
    }

    public Pris getPris(int i)
    {
        return tilknyttedePriser.get(i);
    }

    public void setPriser(ArrayList<Pris> pris)
    {
        this.tilknyttedePriser = pris;
    }

    public void tilføjPris(Pris p)
    {
        tilknyttedePriser.add(p);
    }

    public void fjernPris(Pris p)
    {
        tilknyttedePriser.remove(p);
    }

    public ArrayList<Prisliste> getPrislister()
    {
        ArrayList<Prisliste> prislister = new ArrayList<>();
        for (int i = 0; i < tilknyttedePriser.size(); i++) {
            prislister.add(tilknyttedePriser.get(i).getPrisliste());
        }
        return prislister;
    }

    public int getPant()
    {
        return pant;
    }

    public boolean isRundvisning()
    {
        return false;
    }

    @Override
    public String toString()
    {
        return navn;
    }
}