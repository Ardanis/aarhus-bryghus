package service;

import static org.junit.Assert.assertEquals;
import model.Antal;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;

import org.junit.Before;
import org.junit.Test;

public class ServiceTest {
	
	private Produktgruppe pGruppe;
	private Produkt  testProdukt;
	private Prisliste pListe;
	private Pris testPris;
	private Antal antal;
	
	@Before
	public void initiate(){
		pGruppe = Service.getService().opretProduktGruppe("TestGruppe");
		testProdukt = Service.getService().opretProdukt("Test Øl", "Smager af Test", pGruppe);
		pListe = Service.getService().opretPrisListe("Testliste");
		testPris = Service.getService().opretPris(100, testProdukt, pListe);
		antal = Service.getService().opretAntal(10, testPris);
	}
	
	// Her tester vi om startSalg starter et salg
	
	@Test
	public void test() {
		Service.getService().startSalg(antal);
		assertEquals(1000.00, Service.getService().getIgangvaerendeSalg().getSamletPris(), 0.001);
	}

}
