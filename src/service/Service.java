package service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import model.Antal;
import model.Fustage;
import model.Klip;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;
import model.Regning;
import model.Rundvisning;
import model.Salg;
import model.Sampakning;
import storage.Storage;

public class Service
{
    private static Service service;
    private Salg igangvaerendeSalg;
    private double tempPant = 0;

    private Service()
    {

    }

    /**
     * metoden der sørger for service er Singleton pattern
     *
     * @return service klassen
     */

    public static Service getService()
    {
        if (service == null) {
            service = new Service();
        }
        return service;
    }

    /**
     * Igangsætter et nyt salg
     *
     * @param antal
     *            Den første antals klasse der bliver tilføjet
     */

    public void startSalg(Antal antal)
    {
        if (igangvaerendeSalg != null && igangvaerendeSalg.getManglerAtBetale() == 0) {
            this.afslutSalg();
        }
        if (igangvaerendeSalg == null) {
            igangvaerendeSalg = new Salg(LocalDate.now(), antal);
            igangvaerendeSalg.setManglerAtBetale(igangvaerendeSalg.getSamletPris());
        }
    }

    /**
     * trækker det modtagede beløb fra det som mangler at blive betalt. Hvis
     * hele beløbbet dækker det der mangler at blive betalt, afslutter den
     * salget.
     *
     * @return den samlede pris for salget
     */

    public void afslutSalg()
    {
        Storage.getInstance().tilføjSalg(igangvaerendeSalg);
        igangvaerendeSalg = null;
    }

    public void udlej()
    {
        Storage.getInstance().getAlleUdlejninger().add(igangvaerendeSalg);
        igangvaerendeSalg = null;
    }

    public void hentUdlejning(Salg salg)
    {
        igangvaerendeSalg = salg;
        tempPant = 0;
        for (Antal p : igangvaerendeSalg.getPriser()) {
            tempPant += p.getAntal() * p.getPris().getProdukt().getPant();
        }
        igangvaerendeSalg.setManglerAtBetale(igangvaerendeSalg.getManglerAtBetale() - tempPant);
    }

    /**
     * tilføjer ny antal til salget
     *
     * @param antal
     *            Den nye antal der skal tilføjes til salget
     */
    public void tilføjTilSalg(Antal antal)
    {
        igangvaerendeSalg.tilføjPris(antal);
        igangvaerendeSalg.setManglerAtBetale(igangvaerendeSalg.getSamletPris());
    }

    public void fjernFraSalg(Antal antal)
    {
        igangvaerendeSalg.fjernPris(antal);
        igangvaerendeSalg.setManglerAtBetale(igangvaerendeSalg.getSamletPris());
    }

    public ArrayList<Salg> getSalg()
    {
        return Storage.getInstance().getAlleSalg();
    }

    // ---------------------------------------------------------------------------------
    // Antal Metoder:
    public Antal opretAntal(int antal, Pris pris)
    {
        Antal temp = new Antal(antal, pris);
        return temp;
    }

    // ---------------------------------------------------------------------------------
    // Regning Metoder:
    public void betalMedRegning(Regning r)
    {
        r.tilføjSalg(igangvaerendeSalg);
        this.afslutSalg();
    }

    public void betalRegning(Regning r)
    {
        Storage.getInstance().getAlleRegninger().remove(r);
    }

    public void tilføjRegning(Regning regning)
    {
        Storage.getInstance().tilføjRegning(regning);
    }

    public Regning opretRegning(String kundeNavn)
    {
        Regning temp = new Regning(kundeNavn);
        Service.service.tilføjRegning(temp);
        return temp;
    }

    public ArrayList<Regning> getRegninger()
    {
        return Storage.getInstance().getAlleRegninger();
    }
    // ---------------------------------------------------------------------------------
    // Produkt Metoder:

    /**
     * opretter et nyt produkt og tilf�jer det i storageclassen
     *
     * @param navn
     *            Navnet p� Produkter
     * @param beskrivelse
     *            Beskrivelsen af produktet
     * @param pGruppe
     *            Produktgruppen til Produktet
     * @return det oprettede produkt
     */

    public Produkt opretProdukt(String navn, String beskrivelse, Produktgruppe pGruppe)
    {
        Produkt tempProd = new Produkt(navn, beskrivelse, pGruppe);
        tempProd.getGruppe().tilføjProdukt(tempProd);
        Storage.getInstance().getAlleProdukter().add(tempProd);
        return tempProd;
    }

    public void tilføjProdukt(Produkt produkt, Produktgruppe produktgruppe)
    {
        Storage.getInstance().tilføjProdukt(produkt);
        produktgruppe.tilføjProdukt(produkt);
    }

    public void opdaterProdukt(Produkt produkt, String navn, String beskrivelse,
            Produktgruppe pGruppe)
    {
        produkt.setBeskrivelse(beskrivelse);
        produkt.setGruppe(pGruppe);
        produkt.setNavn(navn);
    }

    public void fjernProdukt(Produkt produkt)
    {
        produkt.getGruppe().fjernProdukt(produkt);
    }

    public ArrayList<Produkt> getProdukter()
    {
        return Storage.getInstance().getAlleProdukter();
    }

    // ---------------------------------------------------------------------------------
    // Pris Metoder:
    /**
     * opretter en ny pris til et produkt derefter tilf�jer det til den g�ldende
     * prisliste i Storage
     *
     * @param pris
     *            Selve prisen p� produktet, dette er en Double, ikke til at
     *            forveksle med en pris klasse
     * @param produkt
     *            Det produkt prisen skal tilf�jes til
     * @param pListe
     *            Prislisten i hvortil prisen skal tilf�jes
     * @return den oprettede Pris klasse
     */

    public Pris opretNyPris(double pris, Produkt produkt, Prisliste pListe)
    {
        Pris tempPris = new Pris(pris, produkt, pListe);
        for (Prisliste p : Storage.getInstance().getAllePrislister()) {
            if (Storage.getInstance().getAllePrislister().contains(pListe)) {
                p.tilføjPris(tempPris);
            }
            produkt.tilføjPris(tempPris);
        }
        return tempPris;
    }

    // ---------------------------------------------------------------------------------
    // Produktgruppe Metoder:
    public Produktgruppe opretProduktGruppe(String navn)
    {
        Produktgruppe tempPGruppe = new Produktgruppe(navn);
        Storage.getInstance().getAlleProduktgrupper().add(tempPGruppe);
        return tempPGruppe;
    }

    public void opdaterProduktgruppe(Produktgruppe pGruppe, Produkt produkt)
    {
        pGruppe.tilføjProdukt(produkt);
    }

    public ArrayList<Produktgruppe> getProduktgrupper()
    {
        return Storage.getInstance().getAlleProduktgrupper();
    }

    public void fjernFraProduktgruppe(Produktgruppe pGruppe, Produkt produkt)
    {
        pGruppe.fjernProdukt(produkt);
        Storage.getInstance().getAlleProdukter().remove(produkt);
    }

    // ---------------------------------------------------------------------------------
    // Prisliste Metoder:
    public Prisliste opretPrisListe(String navn)
    {
        Prisliste pListe = new Prisliste(navn);
        Storage.getInstance().getAllePrislister().add(pListe);
        return pListe;
    }

    public void tilføjTilPrisliste(Prisliste p, Pris pris)
    {
        p.tilføjPris(pris);
    }

    public void fjernPrisFraPrisliste(Prisliste p, Pris pris)
    {
        p.fjernPris(pris);
    }

    public void fjernPrisliste(Prisliste p)
    {
        Storage.getInstance().getAllePrislister().remove(p);
    }

    public ArrayList<Prisliste> getPrislister()
    {
        return Storage.getInstance().getAllePrislister();
    }

    // ---------------------------------------------------------------------------------
    // Metoder til Fustage klassen:
    public Fustage opretFustage(String navn, String beskrivelse, Produktgruppe produktgruppe,
            int volume)
    {
        Fustage temp = new Fustage(navn, beskrivelse, produktgruppe, volume);
        Service.service.tilføjProdukt(temp, produktgruppe);
        return temp;
    }

    // ---------------------------------------------------------------------------------
    // Metoder til Bookinger:

    public Rundvisning opretRundvisning(String navn, String beskrivelse,
            Produktgruppe produktgruppe, LocalDateTime tidspunkt, int gruppeMængde, int studerende)
    {
        Rundvisning temp = new Rundvisning(navn, beskrivelse, produktgruppe, tidspunkt,
                gruppeMængde, studerende);
        Storage.getInstance().getAlleRundvisninger().add(temp);
        return temp;
    }

    public ArrayList<Rundvisning> getBookinger(LocalDate dato)
    {
        ArrayList<Rundvisning> temp = new ArrayList<>();
        ArrayList<Rundvisning> alleRundvisninger = Storage.getInstance().getAlleRundvisninger();
        for (int i = 0; i < alleRundvisninger.size(); i++) {
            if (alleRundvisninger.get(i).getTidspunkt().toLocalDate().equals(dato)) {
                temp.add(alleRundvisninger.get(i));
            }
        }
        return temp;
    }

    public ArrayList<Salg> getUdlejninger(LocalDate dato)
    {
        ArrayList<Salg> temp = new ArrayList<>();
        ArrayList<Salg> alleUdlejninger = Storage.getInstance().getAlleUdlejninger();
        for (int i = 0; i < alleUdlejninger.size(); i++) {
            if (alleUdlejninger.get(i).getSalgsDato().equals(dato)) {
                temp.add(alleUdlejninger.get(i));
            }
        }
        return temp;
    }

    // ---------------------------------------------------------------------------------
    // Metoder til Sampakning klassen:
    public Sampakning opretSampakning(String navn, String beskrivelse, Produktgruppe produktgruppe)
    {
        Sampakning temp = new Sampakning(navn, beskrivelse, produktgruppe);
        Service.service.tilføjProdukt(temp, produktgruppe);

        return temp;
    }

    // ---------------------------------------------------------------------------------
    // Antal Metoder:
    public void redigerAntal(Antal antal, int i)
    {
        antal.setAntal(antal.getAntal() + i);
        igangvaerendeSalg
                .setSamletPris(igangvaerendeSalg.getSamletPris() - (antal.getPris().getPris() * i));
    }

    public void fjernAntal(Antal a)
    {
        igangvaerendeSalg.fjernPris(a);
    }

    public Salg getIgangvaerendeSalg()
    {
        return igangvaerendeSalg;
    }

    // ---------------------------------------------------------------------------------
    // Pris klasse metoder:
    public Pris opretPris(double pris, Produkt produkt, Prisliste pListe)
    {
        Pris tempPris = new Pris(pris, produkt, pListe);
        this.tilføjTilPrisliste(pListe, tempPris);
        produkt.tilføjPris(tempPris);

        return tempPris;
    }

    public ArrayList<Klip> getAlleKlip()
    {
        return Storage.getInstance().getAlleKlip();
    }

    // ---------------------------------------------------------------------------------
    // Storage opfyldning:
    public void initStorage()
    {
        // Produktgrupper:
        Produktgruppe PG1 = Service.getService().opretProduktGruppe("Flaske");

        Produktgruppe PG2 = Service.getService().opretProduktGruppe("Fadøl, 40 cl");

        Produktgruppe PG3 = Service.getService().opretProduktGruppe("Spiritus");

        Produktgruppe PG4 = Service.getService().opretProduktGruppe("Fustage");

        Produktgruppe PG5 = Service.getService().opretProduktGruppe("Kulsyre");

        Produktgruppe PG6 = Service.getService().opretProduktGruppe("Malt");

        Produktgruppe PG7 = Service.getService().opretProduktGruppe("Beklædning");

        Produktgruppe PG8 = Service.getService().opretProduktGruppe("Anlæg");

        Produktgruppe PG9 = Service.getService().opretProduktGruppe("Glas");

        Produktgruppe PG10 = Service.getService().opretProduktGruppe("Sampakninger");

        Produktgruppe PG11 = Service.getService().opretProduktGruppe("Rundvisninger");

        // Produkter:
        // flaske Produktgruppen:
        Produkt p1 = Service.service.opretProdukt("Klosterbryg",
                "Klosterbryg, 6% alk. er udviklet til Øm Kloster museum, i anledning af Øl på Øm.",
                PG1);
        Produkt p2 = Service.service.opretProdukt("Sweet Georgia Brown",
                "øl på 5,5% er mørk, fyldig og med en let sødme, som klæder bitterheden fra de mørke malte og de engelske aromahumler.",
                PG1);
        Produkt p3 = Service.service.opretProdukt("Extra Pilsner",
                "Øl med 5% alkohol, brygget på pilsnermalt, hvedemalt og karamelmalt.", PG1);
        Produkt p4 = Service.service.opretProdukt("Celebration",
                " øl med 5% alkohol, brygget på pilsnermalt, hvedemalt og karamelmalt.", PG1);
        Produkt p5 = Service.service.opretProdukt("Blondie",
                "Den friske øl er mild og blid, og man bliver hurtigt rolig i Blondies selskab. Hun er nu også kun på 5%.",
                PG1);
        Produkt p6 = Service.service.opretProdukt("Forårsbryg",
                "Forårsbryg er en lys Dortmunder type 7% alk.", PG1);
        Produkt p7 = Service.service.opretProdukt("India Pale Ale",
                "En traditionel India Pale Ale på 6%", PG1);
        Produkt p8 = Service.service.opretProdukt("Julebryg",
                "Julebryg fra Aarhus Bryghus med 6% alkohol er brygget på pilsnermalt, hvedemalt, münchenermalt, karamelmalt og farvemalt. ",
                PG1);
        Produkt p9 = Service.service.opretProdukt("Juletønden",
                "Juletønden fra Aarhus Bryghus er en mørk specialøl brygget på bygmalt og sukker, krydret med humle. 8% alk.",
                PG1);
        Produkt p10 = Service.service.opretProdukt("Old Strong Ale",
                "Old Strong Ale er brygget af udvalgte bygmalte, sukker og aromahumle.", PG1);
        Produkt p11 = Service.service.opretProdukt("Fregatten Jylland",
                " 8% alkohol, brygget på Fregatten Jyllandpilsnermalt, münchenermalt, karamelmalt og farvemalt, tilsat en sjat æblesaft, og krydret med en ordentlig nævefuld af aromahumle.",
                PG1);
        Produkt p12 = Service.service.opretProdukt("Imperial Stout",
                "oprindeligt udviklet til Zarens hof i Skt. Petersborg. I 1800-tallet 8% alk.",
                PG1);
        Produkt p13 = Service.service.opretProdukt("Tribute",
                "Tribute er en kraftig Barleywine på 9% alk. Den er en hyldest til det bedste fra Liverpool",
                PG1);
        Produkt p14 = Service.service.opretProdukt("Black Monster",
                "Black Monster på 10% er brygget på de bedste malte og de mest kostbare aromahumler, hvorpå øllet er lagret på Rom-fade. .",
                PG1);

        // fadøl, 40 cl produktgruppen
        Produkt p15 = Service.service.opretProdukt("Klosterbryg",
                "Klosterbryg, 6% alk. er udviklet til Øm Kloster museum, i anledning af Øl på Øm.",
                PG2);
        Produkt p16 = Service.service.opretProdukt("Jazz Classic",
                "En behagelig mørk classic type. Let tilgængelig med en balanceret bitterhed, kun krydret med aromahumlen Perle. 5% alk.",
                PG2);
        Produkt p17 = Service.service.opretProdukt("Extra Pilsner",
                "Øl med 5% alkohol, brygget på pilsnermalt, hvedemalt og karamelmalt.", PG2);
        Produkt p18 = Service.service.opretProdukt("Celebration",
                "Øl med 5% alkohol, brygget på pilsnermalt, hvedemalt og karamelmalt.", PG2);
        Produkt p19 = Service.service.opretProdukt("Blondie",
                "Den friske øl er mild og blid, og man bliver hurtigt rolig i Blondies selskab. Hun er nu også kun på 5%.",
                PG2);
        Produkt p20 = Service.service.opretProdukt("Forårsbryg",
                "Forårsbryg er en lys Dortmunder type 7% alk.", PG2);
        Produkt p21 = Service.service.opretProdukt("India Pale Ale",
                "En traditionel India Pale Ale på 6%", PG2);
        Produkt p22 = Service.service.opretProdukt("Julebryg",
                "Julebryg fra Aarhus Bryghus med 6% alkohol er brygget på pilsnermalt, hvedemalt, münchenermalt, karamelmalt og farvemalt. ",
                PG2);
        Produkt p23 = Service.service.opretProdukt("Imperial Stout",
                "Oprindeligt udviklet til Zarens hof i Skt. Petersborg. I 1800-tallet 8% alk.",
                PG2);
        Produkt p24 = Service.service.opretProdukt("Special", "Der er ikke en der hedder special",
                PG2);
        Produkt p25 = Service.service.opretProdukt("Æblebrus", "Æblebrus", PG2);
        Produkt p26 = Service.service.opretProdukt("Chips", "Chips, flere varianter", PG2);
        Produkt p27 = Service.service.opretProdukt("Peanuts", "Peanuts, flere varianter", PG2);
        Produkt p28 = Service.service.opretProdukt("Cola", "Coca Cola", PG2);
        Produkt p29 = Service.service.opretProdukt("Nikoline", "Appelsinvand", PG2);
        Produkt p30 = Service.service.opretProdukt("7-Up", "Sports sodavand", PG2);
        Produkt p31 = Service.service.opretProdukt("Vand", "Naturligt kildevand", PG2);

        // Spiritus Produktgruppe:
        Produkt p32 = Service.service.opretProdukt("Spirit of Aarhus", "Meget stærk øl på 11%.",
                PG3);
        Produkt p33 = Service.service.opretProdukt("SOA med pind",
                "Meget stærk øl på 11%. Hvor der er en egesplint i flasken", PG3);
        Produkt p34 = Service.service.opretProdukt("Whiskey", "Tappet ved 43%.", PG3);
        Produkt p35 = Service.service.opretProdukt("Liquor of Aarhus",
                "Liquor of Aarhus er en krydret appelsin-likør, inspireret af Drambuie og Cointreau. Den er på 30%, tappet i høje, slanke 35 cl flasker.",
                PG3);

        // Fustage Produktgruppe:
        Fustage p36 = Service.service.opretFustage("Klosterbryg",
                "Klosterbryg, 6% alk. er udviklet til Øm Kloster museum, i anledning af Øl på Øm.",
                PG4, 20);
        Fustage p37 = Service.service.opretFustage("Jazz Classic",
                "En behagelig mørk classic type. Let tilgængelig med en balanceret bitterhed, kun krydret med aromahumlen Perle. 5% alk.",
                PG4, 25);
        Fustage p38 = Service.service.opretFustage("Extra Pilsner",
                "Øl med 5% alkohol, brygget på pilsnermalt, hvedemalt og karamelmalt.", PG4, 25);
        Fustage p39 = Service.service.opretFustage("Celebration",
                "Øl med 5% alkohol, brygget på pilsnermalt, hvedemalt og karamelmalt.", PG4, 20);
        Fustage p40 = Service.service.opretFustage("Blondie",
                "Den friske øl er mild og blid, og man bliver hurtigt rolig i Blondies selskab. Hun er nu også kun på 5%.",
                PG4, 25);
        Fustage p41 = Service.service.opretFustage("Forårsbryg",
                "Forårsbryg er en lys Dortmunder type 7% alk.", PG4, 20);
        Fustage p42 = Service.service.opretFustage("India Pale Ale",
                "En traditionel India Pale Ale på 6%", PG4, 20);
        Fustage p43 = Service.service.opretFustage("Julebryg",
                "Julebryg fra Aarhus Bryghus med 6% alkohol er brygget på pilsnermalt, hvedemalt, münchenermalt, karamelmalt og farvemalt. ",
                PG4, 20);
        Fustage p44 = Service.service.opretFustage("Imperial Stout",
                "Oprindeligt udviklet til Zarens hof i Skt. Petersborg. I 1800-tallet 8% alk.", PG4,
                20);
        Fustage p45 = Service.service.opretFustage("Special", "Ingen beskrivelse", PG4, 20);

        // Kulsyre Produktgruppe:
        Fustage p46 = Service.service.opretFustage("6 kg.", "6 kg. kulsyre", PG5, 0);
        Fustage p47 = Service.service.opretFustage("Pant", "Bruges til udlejning", PG5, 0);
        Fustage p48 = Service.service.opretFustage("4 kg.", "4 kg. kulsyre", PG5, 0);
        Fustage p49 = Service.service.opretFustage("10 kg.", "10 kg. kulsyre", PG5, 0);

        // Malt Produktgruppe:
        Produkt p50 = Service.service.opretProdukt("25 kg. sæk", "25 kg. maltsæk", PG6);

        // Beklædning produktgruppe:
        Produkt p51 = Service.service.opretProdukt("T-shirt", "T-shirt med Aarhus Bryghus logo.",
                PG7);
        Produkt p52 = Service.service.opretProdukt("Polo", "Polo med Aarhus Bryghus logo.", PG7);
        Produkt p53 = Service.service.opretProdukt("Cap", "Cap med Aarhus Bryghus logo.", PG7);

        // Anlæg Produktgruppe:

        Produkt p54 = Service.service.opretProdukt("1-hane", "1 hane med øl", PG8);
        Produkt p55 = Service.service.opretProdukt("2-haner", "2 haner med øl", PG8);
        Produkt p56 = Service.service.opretProdukt("Bar med flere haner",
                "Flere haner aftales med Bryghuset", PG8);
        Produkt p57 = Service.service.opretProdukt("Levering", "Levering af bestilte haner", PG8);
        Produkt p58 = Service.service.opretProdukt("Krus", "Plastkrus 50 stk.", PG8);

        // Glas Produktgruppe:
        Produkt p59 = Service.service.opretProdukt("Uanset størrelse",
                "De er til låns og skal bare returneres rene", PG9);

        // Sampakninger Produktgruppe:
        Sampakning p60 = Service.service.opretSampakning("Gaveæske 2 Øl, 2 Glas", "2 øl og 2 glas",
                PG10);
        Sampakning p61 = Service.service.opretSampakning("Gaveæske 4 Øl", "4 øl i gaveæske", PG10);
        Sampakning p62 = Service.service.opretSampakning("Trækasse 6 Øl", "6 øl i gaveæske", PG10);
        Sampakning p63 = Service.service.opretSampakning("Gavekurv 6 Øl, 2 Glas",
                "6 øl og 2 glas i gavekurv", PG10);
        Sampakning p64 = Service.service.opretSampakning("Trækasse 6 Øl, 6 Glas",
                "6 øl og 6 glas i trækasse", PG10);
        Sampakning p65 = Service.service.opretSampakning("Trækasse 12 Øl", "12 øl i trækasse",
                PG10);
        Sampakning p66 = Service.service.opretSampakning("Papkasse 12 Øl", "12 øl i papkasse",
                PG10);

        // Rundvisning Produktgruppe:
        Rundvisning p67 = Service.getService().opretRundvisning("EAAA",
                "Der kommer en flok studerende i forbindelse med et projekt", PG11,
                LocalDateTime.now(), 28, 25);

        // Prislister
        Prisliste pl1 = Service.service.opretPrisListe("FredagsBar");
        Prisliste pl2 = Service.service.opretPrisListe("Butik");
        // ---------------------------------------------------------------------------------
        // Produkter Priser:
        // Flaske Fredagsbar
        Pris pris1 = Service.service.opretPris(50, p1, pl1);
        Pris pris2 = Service.service.opretPris(50, p2, pl1);
        Service.service.opretPris(50, p3, pl1);
        Service.service.opretPris(50, p4, pl1);
        Service.service.opretPris(50, p5, pl1);
        Service.service.opretPris(50, p6, pl1);
        Service.service.opretPris(50, p7, pl1);
        Service.service.opretPris(50, p8, pl1);
        Service.service.opretPris(50, p9, pl1);
        Service.service.opretPris(50, p10, pl1);
        Service.service.opretPris(50, p11, pl1);
        Service.service.opretPris(50, p12, pl1);
        Service.service.opretPris(50, p13, pl1);
        Service.service.opretPris(50, p14, pl1);

        // Flaske Butik:
        Service.service.opretPris(36, p1, pl2);
        Service.service.opretPris(36, p2, pl2);
        Service.service.opretPris(36, p3, pl2);
        Service.service.opretPris(36, p4, pl2);
        Service.service.opretPris(36, p5, pl2);
        Service.service.opretPris(36, p6, pl2);
        Service.service.opretPris(36, p7, pl2);
        Service.service.opretPris(36, p8, pl2);
        Service.service.opretPris(36, p9, pl2);
        Service.service.opretPris(36, p10, pl2);
        Service.service.opretPris(36, p11, pl2);
        Service.service.opretPris(36, p12, pl2);
        Service.service.opretPris(36, p13, pl2);
        Service.service.opretPris(50, p14, pl2);
        // ---------------------------------------------------------------------------------
        // Fadøl, 40 cl, Fredagsbar:
        Service.service.opretPris(30, p15, pl1);
        Service.service.opretPris(30, p16, pl1);
        Service.service.opretPris(30, p17, pl1);
        Service.service.opretPris(30, p18, pl1);
        Service.service.opretPris(30, p19, pl1);
        Service.service.opretPris(30, p20, pl1);
        Service.service.opretPris(30, p21, pl1);
        Service.service.opretPris(30, p22, pl1);
        Service.service.opretPris(30, p23, pl1);
        Service.service.opretPris(30, p24, pl1);
        Service.service.opretPris(15, p25, pl1);
        Service.service.opretPris(10, p26, pl1);
        Service.service.opretPris(10, p27, pl1);
        Service.service.opretPris(15, p28, pl1);
        Service.service.opretPris(15, p29, pl1);
        Service.service.opretPris(15, p30, pl1);
        Service.service.opretPris(10, p31, pl1);

        // Fadøl, 40 cl, Fredagsbar:
        // N/A

        // ---------------------------------------------------------------------------------
        // Spiritus, Fredagsbar:
        Service.service.opretPris(300, p32, pl1);
        Service.service.opretPris(350, p33, pl1);
        Service.service.opretPris(50, p34, pl1);
        Service.service.opretPris(175, p35, pl1);

        // Spiritus Butik:
        Service.service.opretPris(300, p32, pl2);
        Service.service.opretPris(350, p33, pl2);
        Service.service.opretPris(50, p34, pl2);
        Service.service.opretPris(175, p35, pl2);
        // ---------------------------------------------------------------------------------
        // Fustage, Fredagsbar:
        // N/A

        // Fustage, Butik:
        Pris pris7 = Service.service.opretPris(775, p36, pl2);
        Pris pris8 = Service.service.opretPris(625, p37, pl2);
        Service.service.opretPris(575, p38, pl2);
        Service.service.opretPris(775, p39, pl2);
        Service.service.opretPris(700, p40, pl2);
        Service.service.opretPris(775, p41, pl2);
        Service.service.opretPris(775, p42, pl2);
        Service.service.opretPris(775, p43, pl2);
        Service.service.opretPris(775, p44, pl2);
        Service.service.opretPris(775, p45, pl2);
        // ---------------------------------------------------------------------------------
        // Kulsyre, Fredagsbar:
        Pris pris9 = Service.service.opretPris(400, p46, pl1);
        Service.service.opretPris(100, p47, pl1);
        Service.service.opretPris(264, p48, pl1);
        Service.service.opretPris(666, p49, pl1);

        // Kulsyre, Butik:
        Service.service.opretPris(400, p46, pl2);
        Service.service.opretPris(100, p47, pl2);
        Service.service.opretPris(264, p48, pl2);
        Service.service.opretPris(666, p49, pl2);
        // ---------------------------------------------------------------------------------
        // Malt, Fredagsbar:
        // N/A

        // Malt, Butik:
        Service.service.opretPris(300, p50, pl1);
        // ---------------------------------------------------------------------------------
        // Beklædning, Fredagsbar:
        Service.service.opretPris(70, p51, pl1);
        Service.service.opretPris(100, p52, pl1);
        Service.service.opretPris(30, p53, pl1);

        // Beklædning, Butik:
        Service.service.opretPris(70, p51, pl2);
        Service.service.opretPris(100, p52, pl2);
        Service.service.opretPris(30, p53, pl2);
        // ---------------------------------------------------------------------------------
        // Anlæg, Fredagsbar:
        // N/A

        // Anlæg, Butik:
        Service.service.opretPris(250, p54, pl2);
        Service.service.opretPris(400, p55, pl2);
        Service.service.opretPris(500, p56, pl2);
        Service.service.opretPris(500, p57, pl2);
        Service.service.opretPris(60, p58, pl2);
        // ---------------------------------------------------------------------------------
        // Glas, Fredagsbar:
        // N/A

        // Glas, Butik:
        Service.service.opretPris(15, p59, pl2);
        // ---------------------------------------------------------------------------------
        // Sampakninger, Fredagsbar:
        Service.service.opretPris(100, p60, pl1);
        Service.service.opretPris(130, p61, pl1);
        Service.service.opretPris(240, p62, pl1);
        Service.service.opretPris(250, p63, pl1);
        Service.service.opretPris(290, p64, pl1);
        Service.service.opretPris(390, p65, pl1);
        Service.service.opretPris(360, p66, pl1);

        // Sampakninger, Butik:
        Service.service.opretPris(100, p60, pl2);
        Service.service.opretPris(130, p61, pl2);
        Service.service.opretPris(240, p62, pl2);
        Service.service.opretPris(250, p63, pl2);
        Service.service.opretPris(290, p64, pl2);
        Service.service.opretPris(390, p65, pl2);
        Service.service.opretPris(360, p66, pl2);

        // Rundvisninger, Butik:
        Service.service.opretPris(0, p67, pl2);
        // ---------------------------------------------------------------------------------
        // Regninger:
        Regning regning1 = Service.service.opretRegning("Arla");

        // ---------------------------------------------------------------------------------
        // Antal:
        Antal antal1 = Service.service.opretAntal(2, pris1);
        Antal antal2 = Service.service.opretAntal(3, pris2);
        Antal antal3 = Service.service.opretAntal(4, pris7);
        Antal antal4 = Service.getService().opretAntal(5, pris8);
        Antal antal5 = Service.getService().opretAntal(1, pris9);

        // ---------------------------------------------------------------------------------
        // Salg:
        Service.service.startSalg(antal1);
        Service.service.tilføjTilSalg(antal2);
        Service.service.betalMedRegning(regning1);

        // ----------------------------------------------------------------------------------
        // Rundvisning
        Service.getService().startSalg(antal3);
        Service.getService().tilføjTilSalg(antal4);
        Service.getService().tilføjTilSalg(antal5);
        Service.getService().udlej();
    }
