package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Produktgruppe;
import service.Service;

public class OpretProduktWindow extends Stage
{

    public OpretProduktWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -------------------------------------------------------------------------

    private TextField txfNavn;
    private TextArea txaBeskrivelse;
    private ComboBox<Produktgruppe> cmbProduktgrupper;
    private Label lblProdukter, lblError;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        txfNavn = new TextField("Navn på Produkt");
        pane.add(txfNavn, 0, 0);

        txaBeskrivelse = new TextArea();
        pane.add(txaBeskrivelse, 0, 1);

        lblProdukter = new Label("Vælg Produktgruppe");
        pane.add(lblProdukter, 2, 0);

        lblError = new Label();
        lblError.setStyle("-fx-text-fill: red");
        pane.add(lblError, 1, 1);

        cmbProduktgrupper = new ComboBox<>();
        pane.add(cmbProduktgrupper, 1, 0);
        for (int i = 0; i < Service.getService().getProduktgrupper().size(); i++) {
            cmbProduktgrupper.getItems().add(Service.getService().getProduktgrupper().get(i));
        }

        HBox hbox1 = new HBox(20);
        pane.add(hbox1, 0, 2, 2, 1);
        hbox1.setAlignment(Pos.BASELINE_CENTER);

        Button btnOK = new Button("Gem");
        hbox1.getChildren().add(btnOK);
        btnOK.setOnAction(event -> this.gemAction());

        Button btnFortryd = new Button("Annuller");
        hbox1.getChildren().add(btnFortryd);
        btnFortryd.setOnAction(event -> this.cancelAction());
    }

    // -----------------------------------------------------
    // Button actions
    private void cancelAction()
    {
        this.hide();
    }

    private void gemAction()
    {
        if (cmbProduktgrupper.getSelectionModel().getSelectedItem() != null) {
            Produktgruppe produktgruppe = cmbProduktgrupper.getSelectionModel().getSelectedItem();
            if (txfNavn.getText().equals("Navn på Produkt")) {
                lblError.setText("Navn er ugyldigt!");
            } else {
                String navn = txfNavn.getText();
                if (txaBeskrivelse.getLength() == 0) {
                    lblError.setText("Beskrivelse er ugyldigt!");
                } else {
                    String beskrivelse = txaBeskrivelse.getText();
                    Service.getService().opretProdukt(navn, beskrivelse, produktgruppe);
                    this.hide();
                }
            }
        } else {
            lblError.setText("Produktgruppe er ugyldigt!");
        }
    }
}