package gui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Prisliste;
import model.Produkt;
import service.Service;

public class PrislisterPane extends GridPane
{

    private ListView<Prisliste> lvwPrislister;
    private ListView<Produkt> lvwProdukt;
    // private ListView<Prisliste> lvwPrisliste;
    private TextField txfNavn;
    private TextArea txaBeskrivelse;
    private Label lbl1, lbl2, lbl3, lbl4;
    private Button btn1, btn2;

    public PrislisterPane()
    {
        this.setGridLinesVisible(false);
        this.setPadding(new Insets(30));
        this.setHgap(10);
        this.setVgap(10);

        this.setPrefSize(840, 456);
        lbl1 = new Label("Prislister");
        this.add(lbl1, 1, 0);
        lbl2 = new Label("Tilknyttede Produkter");
        this.add(lbl2, 2, 0);
        lbl3 = new Label("Navn");
        this.add(lbl3, 3, 0);
        lbl4 = new Label("Beskrivelse");
        this.add(lbl4, 3, 2);

        btn1 = new Button("Opret Prisliste");
        this.add(btn1, 0, 1);
        btn1.setOnAction(event -> this.opretAction());
        btn2 = new Button("Rediger Prisliste");
        this.add(btn2, 0, 2);
        btn2.setOnAction(event -> this.redigerAction());

        lvwPrislister = new ListView<>();
        lvwPrislister.setMinSize(236, 350);
        lvwPrislister.setMaxSize(236, 350);
        this.add(lvwPrislister, 1, 1, 1, 7);
        for (int i = 0; i < Service.getService().getPrislister().size(); i++) {
            lvwPrislister.getItems().add(Service.getService().getPrislister().get(i));
        }
        ChangeListener<Prisliste> listener = (ov, oldPrisliste, newPrisliste) -> this
                .opdaterPrislisteAction();
        lvwPrislister.getSelectionModel().selectedItemProperty().addListener(listener);

        lvwProdukt = new ListView<>();
        lvwProdukt.setMinSize(195, 350);
        lvwProdukt.setMaxSize(195, 350);
        this.add(lvwProdukt, 2, 1, 1, 7);
        for (int i = 0; i < Service.getService().getProdukter().size(); i++) {
            lvwProdukt.getItems().add(Service.getService().getProdukter().get(i));
        }
        ChangeListener<Produkt> listener2 = (ov, oldProdukt, newProdukt) -> this
                .valgteProduktSkiftet();
        lvwProdukt.getSelectionModel().selectedItemProperty().addListener(listener2);

        txfNavn = new TextField();
        this.add(txfNavn, 3, 1);
        txfNavn.setEditable(false);
        txfNavn.setMaxSize(157, 25);
        txfNavn.setMinSize(157, 25);

        txaBeskrivelse = new TextArea();
        this.add(txaBeskrivelse, 3, 3, 1, 3);
        txaBeskrivelse.setEditable(false);
        txaBeskrivelse.setMinSize(157, 176);
        txaBeskrivelse.setMaxSize(157, 176);
    }

    // ----------------------------------------------------------------
    // Button actions
    private void opretAction()
    {
        OpretPrislisteWindow win = new OpretPrislisteWindow("Opret Ny Prisliste");
        win.showAndWait();
        this.opdaterAction();
    }

    private void redigerAction()
    {
        RedigerPrislisteWindow win = new RedigerPrislisteWindow("Rediger Prisliste",
                lvwPrislister.getSelectionModel().getSelectedItem());
        win.showAndWait();

        this.opdaterAction();
    }

    private void valgteProduktSkiftet()
    {
        this.opdaterAction();
    }

    private void opdaterPrislisteAction()
    {
        txfNavn.clear();
        txaBeskrivelse.clear();

        Prisliste prisliste = lvwPrislister.getSelectionModel().getSelectedItem();
        if (prisliste != null) {
            int index = lvwPrislister.getSelectionModel().getSelectedIndex();
            lvwProdukt.getItems().clear();
            Platform.runLater(() -> {
                lvwProdukt.getItems()
                        .setAll(Service.getService().getPrislister().get(index).getProdukter());
            });
        }
    }

    private void opdaterAction()
    {
        Produkt produkt = lvwProdukt.getSelectionModel().getSelectedItem();
        if (produkt != null) {
            int index = lvwProdukt.getSelectionModel().getSelectedIndex();
            txfNavn.setText(lvwProdukt.getItems().get(index).getNavn());
            txaBeskrivelse.setText(lvwProdukt.getItems().get(index).getBeskrivelse());
        } else {
            lvwProdukt.getItems().setAll(Service.getService().getProdukter());
            lvwPrislister.getItems().setAll(Service.getService().getPrislister());
        }
    }
}