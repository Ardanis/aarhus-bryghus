package gui;

import java.time.LocalDateTime;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Produktgruppe;
import model.Rundvisning;
import service.Service;

public class OpretRundvisningWindow extends Stage
{
    public OpretRundvisningWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -------------------------------------------------------------------------

    private TextField txfNavn, txfGruppe, txfStuderende, txfTime, txfMinut;
    private TextArea txaBeskrivelse;
    private DatePicker dpDato;
    private Button btnGem, btnAnnuller;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        txfNavn = new TextField("Navn");
        pane.add(txfNavn, 0, 0);

        txfGruppe = new TextField("Antal");
        pane.add(txfGruppe, 1, 0);

        txfStuderende = new TextField("Antal Studerende");
        pane.add(txfStuderende, 2, 0);

        txfTime = new TextField("Time");
        pane.add(txfTime, 0, 1);
        GridPane.setHalignment(txfTime, HPos.LEFT);

        txfMinut = new TextField("Minut");
        pane.add(txfMinut, 1, 1);
        GridPane.setHalignment(txfMinut, HPos.RIGHT);

        dpDato = new DatePicker();
        pane.add(dpDato, 2, 1);

        txaBeskrivelse = new TextArea("Beskrivelse af Rundvisning");
        pane.add(txaBeskrivelse, 0, 2, 1, 2);

        btnGem = new Button("Gem");
        pane.add(btnGem, 0, 4);
        GridPane.setHalignment(btnGem, HPos.RIGHT);
        btnGem.setOnAction(event -> this.gemAction());

        btnAnnuller = new Button("Annuller");
        pane.add(btnAnnuller, 1, 4);
        GridPane.setHalignment(btnAnnuller, HPos.LEFT);
        btnAnnuller.setOnAction(event -> this.closeWindowAction());
    }

    private void closeWindowAction()
    {
        this.hide();
    }

    private void gemAction()
    {
        String navn = txfNavn.getText();
        String beskrivelse = txaBeskrivelse.getText();
        Produktgruppe produktgruppe = Service.getService().getProduktgrupper().get(10);
        LocalDateTime tidspunkt = LocalDateTime.parse(
                dpDato.getValue().toString() + "T" + txfTime.getText() + ":" + txfMinut.getText());
        int gruppeMængde = Integer.parseInt(txfGruppe.getText());
        int studerende;
        if (txfStuderende.getText().equals("Antal Studerende")) {
            studerende = 0;
        } else {
            studerende = Integer.parseInt(txfStuderende.getText());
        }
        Integer.parseInt(txfStuderende.getText());
        Rundvisning temp = Service.getService().opretRundvisning(navn, beskrivelse, produktgruppe,
                tidspunkt, gruppeMængde, studerende);
        Service.getService().opretPris(0, temp, Service.getService().getPrislister().get(1));
        this.hide();
    }
}