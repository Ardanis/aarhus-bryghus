package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import service.Service;

public class OpretPrislisteWindow extends Stage
{

    public OpretPrislisteWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -------------------------------------------------------------------------

    private TextField txfNavn;
    private Label lblError;
    private Button btnGem, btnAnnuller;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        lblError = new Label();
        lblError.setStyle("-fx-text-fill: red");
        pane.add(lblError, 0, 1);

        txfNavn = new TextField("Navn");
        pane.add(txfNavn, 0, 0);

        btnGem = new Button("Gem");
        pane.add(btnGem, 0, 2);
        GridPane.setHalignment(btnGem, HPos.RIGHT);
        btnGem.setOnAction(event -> this.gemAction());

        btnAnnuller = new Button("Annuller");
        pane.add(btnAnnuller, 2, 2);
        btnAnnuller.setOnAction(event -> this.closeWindowAction());
    }

    // -------------------------------------------------------------------------
    // Button actions
    private void gemAction()
    {
        Boolean eksisterer = false;
        if (!(txfNavn.equals("Navn"))) {
            for (int i = 0; i < Service.getService().getPrislister().size(); i++) {
                if (Service.getService().getPrislister().get(i).getNavn()
                        .equals(txfNavn.getText())) {
                    lblError.setText("Prisliste eksisterer allerede");
                    eksisterer = true;
                }
            }
            if (!eksisterer) {
                Service.getService().opretPrisListe(txfNavn.getText());
                this.hide();
            }
        }
    }

    private void closeWindowAction()
    {
        this.hide();
    }
}