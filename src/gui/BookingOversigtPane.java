package gui;

import java.time.LocalDate;
import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import model.Antal;
import model.Produkt;
import model.Rundvisning;
import model.Salg;
import service.Service;

public class BookingOversigtPane extends GridPane
{

    private ListView<Rundvisning> lvwBookinger;
    private ListView<Salg> lvwUdlejninger;
    private ListView<Produkt> lvwProdukter;
    private TextArea txaBeskrivelse;
    private DatePicker dpDato;
    private Label lbl1, lbl2, lbl3, lbl4;
    private Button btn1, btn2;

    public BookingOversigtPane()
    {
        this.setGridLinesVisible(false);
        this.setPadding(new Insets(30));
        this.setHgap(10);
        this.setVgap(10);

        this.setPrefSize(840, 456);

        lbl1 = new Label("Bookinger på valgte dato");
        this.add(lbl1, 1, 0);

        lbl2 = new Label("Beskrivelse");
        this.add(lbl2, 2, 0);

        lbl3 = new Label("Udlejning på valgte Dato");
        this.add(lbl3, 1, 2);

        lbl4 = new Label("Udlejet produkter");
        this.add(lbl4, 2, 2);

        dpDato = new DatePicker();
        this.add(dpDato, 0, 0);
        dpDato.setOnAction(event -> this.hentBookingerAction(dpDato.getValue()));

        lvwBookinger = new ListView<>();
        lvwBookinger.setPrefSize(236, 350);
        this.add(lvwBookinger, 1, 1);
        ChangeListener<Rundvisning> listener = (ov, oldProdukt, newProdukt) -> this.opdaterAction();
        lvwBookinger.getSelectionModel().selectedItemProperty().addListener(listener);

        lvwUdlejninger = new ListView<>();
        lvwUdlejninger.setPrefSize(236, 350);
        this.add(lvwUdlejninger, 1, 3);
        ChangeListener<Salg> listener2 = (ov, oldProdukt, newProdukt) -> this
                .opdaterProdukterAction();
        lvwUdlejninger.getSelectionModel().selectedItemProperty().addListener(listener2);

        lvwProdukter = new ListView<>();
        lvwProdukter.setPrefSize(236, 350);
        this.add(lvwProdukter, 2, 3);

        txaBeskrivelse = new TextArea();
        this.add(txaBeskrivelse, 2, 1);
        txaBeskrivelse.setEditable(false);

        btn1 = new Button("Betal Rundvisning");
        this.add(btn1, 1, 4);
        btn1.setMinSize(200, 148);
        btn1.setOnAction(event -> this.betalAction());

        btn2 = new Button("Betal Udlejning");
        this.add(btn2, 2, 4);
        btn2.setMinSize(200, 148);
        btn2.setOnAction(event -> this.betalUdlejningAction());
    }

    private void betalUdlejningAction()
    {
        lvwUdlejninger.getSelectionModel().getSelectedItem().setBetalt(true);
        Service service = Service.getService();
        service.hentUdlejning(lvwUdlejninger.getSelectionModel().getSelectedItem());
        VælgBetalingsFormWindow win = new VælgBetalingsFormWindow("Vælg Betalingsform");
        win.showAndWait();
        lvwBookinger.getItems().clear();
        lvwProdukter.getItems().clear();
        lvwUdlejninger.getItems().clear();
        txaBeskrivelse.clear();
        dpDato.setValue(null);
    }
    /*--------------------------------------------------------------------------------------------*/

    private void opdaterProdukterAction()
    {
        Salg udlejning = lvwUdlejninger.getSelectionModel().getSelectedItem();
        if (udlejning != null) {
            ArrayList<Produkt> temp = new ArrayList<>();
            for (int i = 0; i < udlejning.getPriser().size(); i++) {
                temp.add(udlejning.getPriser().get(i).getPris().getProdukt());
            }
            lvwProdukter.getItems().setAll(temp);
            if (udlejning.isBetalt()) {
                btn2.setDisable(true);
            } else {
                btn2.setDisable(false);
            }
        }
    }

    private void hentBookingerAction(LocalDate dato)
    {
        lvwBookinger.getItems().setAll(Service.getService().getBookinger(dato));
        lvwUdlejninger.getItems().setAll(Service.getService().getUdlejninger(dato));
    }

    private void opdaterAction()
    {
        Rundvisning booking = lvwBookinger.getSelectionModel().getSelectedItem();
        if (booking != null) {
            txaBeskrivelse.setText(booking.getBeskrivelse());
            if (booking.isBetalt()) {
                btn1.setDisable(true);
            } else {
                btn1.setDisable(false);
            }
        }
    }

    private void betalAction()
    {
        lvwBookinger.getSelectionModel().getSelectedItem().setBetalt(true);
        Service service = Service.getService();
        Antal antal = service.opretAntal(1,
                lvwBookinger.getSelectionModel().getSelectedItem().getPris(0));
        service.startSalg(antal);
        service.getIgangvaerendeSalg()
                .setSamletPris(lvwBookinger.getSelectionModel().getSelectedItem().getPris());
        service.getIgangvaerendeSalg()
                .setManglerAtBetale(service.getIgangvaerendeSalg().getSamletPris());
        VælgBetalingsFormWindow win = new VælgBetalingsFormWindow("Vælg Betalingsform");
        win.showAndWait();
        lvwBookinger.getItems().clear();
        lvwProdukter.getItems().clear();
        lvwUdlejninger.getItems().clear();
        txaBeskrivelse.clear();
        dpDato.setValue(null);
    }
}