package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Produkt;
import service.Service;

public class OpretProduktgruppeWindow extends Stage
{

    public OpretProduktgruppeWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -------------------------------------------------------------------------

    private TextField txfNavn;
    private ListView<Produkt> lvwAlleProdukter, lvwTilføjedeProdukter;
    private Label lblAlleProdukter, lblTilføjedeProdukter;
    private Button btnGem, btnAnnuller, btnTilføj, btnFjern;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        lblAlleProdukter = new Label("Alle Produkter:");
        pane.add(lblAlleProdukter, 0, 1);

        lblTilføjedeProdukter = new Label("Tilføjede Produkter");
        pane.add(lblTilføjedeProdukter, 2, 1);

        txfNavn = new TextField("Navn");
        pane.add(txfNavn, 0, 0);

        lvwAlleProdukter = new ListView<>();
        pane.add(lvwAlleProdukter, 0, 2, 1, 4);

        lvwTilføjedeProdukter = new ListView<>();
        pane.add(lvwTilføjedeProdukter, 2, 2, 1, 4);

        btnTilføj = new Button(">");
        pane.add(btnTilføj, 1, 4);

        btnFjern = new Button("<");
        pane.add(btnFjern, 1, 5);
        GridPane.setValignment(btnFjern, VPos.TOP);

        btnGem = new Button("Gem");
        pane.add(btnGem, 0, 6);
        GridPane.setHalignment(btnGem, HPos.RIGHT);
        btnGem.setOnAction(event -> this.gemAction());

        btnAnnuller = new Button("Annuller");
        pane.add(btnAnnuller, 2, 6);
        btnAnnuller.setOnAction(event -> this.closeWindowAction());
    }

    // -------------------------------------------------------------------------
    // Button actions
    private void gemAction()
    {
        if (!(txfNavn.equals("Navn"))) {
            Service.getService().opretProduktGruppe(txfNavn.getText());
            this.hide();
        }
    }

    private void closeWindowAction()
    {
        this.hide();
    }
}