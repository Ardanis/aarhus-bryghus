package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Regning;
import model.Salg;
import service.Service;

public class RegningPane extends GridPane
{

    private ListView<Regning> lvwRegninger;
    private ListView<Salg> lvwSalg;
    private TextField txfNavn, txfSamletPris;
    private Label lbl1, lbl2, lbl3, lbl4;
    private Button btn1;

    public RegningPane()
    {
        this.setGridLinesVisible(false);
        this.setPadding(new Insets(30));
        this.setHgap(10);
        this.setVgap(10);

        this.setPrefSize(840, 456);
        lbl1 = new Label("Regninger");
        this.add(lbl1, 1, 0);
        lbl2 = new Label("Salg");
        this.add(lbl2, 2, 0);
        lbl3 = new Label("Kunde");
        this.add(lbl3, 3, 0);
        lbl4 = new Label("Samlet Pris");
        this.add(lbl4, 3, 2);

        btn1 = new Button("Betal Regning");
        this.add(btn1, 3, 4);
        btn1.setMinSize(200, 148);

        lvwRegninger = new ListView<>();
        lvwRegninger.setMinSize(236, 350);
        lvwRegninger.setMaxSize(236, 350);
        this.add(lvwRegninger, 1, 1, 1, 7);
        for (int i = 0; i < Service.getService().getRegninger().size(); i++) {
            lvwRegninger.getItems().add(Service.getService().getRegninger().get(i));
        }
        ChangeListener<Regning> listener = (ov, oldRegning, newRegning) -> this.opdaterAction();
        lvwRegninger.getSelectionModel().selectedItemProperty().addListener(listener);

        lvwSalg = new ListView<>();
        lvwSalg.setMinSize(195, 350);
        lvwSalg.setMaxSize(195, 350);
        this.add(lvwSalg, 2, 1, 1, 7);

        txfNavn = new TextField();
        this.add(txfNavn, 3, 1);
        txfNavn.setEditable(false);
        txfNavn.setMaxSize(157, 25);
        txfNavn.setMinSize(157, 25);

        txfSamletPris = new TextField();
        this.add(txfSamletPris, 3, 3);
        txfSamletPris.setEditable(false);
        txfSamletPris.setMaxSize(157, 25);
        txfSamletPris.setMinSize(157, 25);
    }
    /*--------------------------------------------------------------------------------------------*/
    // Button Actions:

    private void opdaterAction()
    {
        lvwSalg.getItems().clear();
        Regning regning = lvwRegninger.getSelectionModel().getSelectedItem();
        if (regning != null) {
            int index = lvwRegninger.getSelectionModel().getSelectedIndex();
            txfNavn.setText(lvwRegninger.getItems().get(index).getKundeNavn());
            txfSamletPris.setText(lvwRegninger.getItems().get(index).getSamletPris() + "");
            lvwSalg.getItems().addAll(regning.getTilknyttedeSalg());
        }
    }

    public void opdaterAlt()
    {
        lvwRegninger.getItems().setAll(Service.getService().getRegninger());
    }

}