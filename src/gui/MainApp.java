package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.Service;

public class MainApp extends Application
{
    public Tab tabSalgPane, tabProdukter, tabPrisLister, tabStatistik, tabRegningPane,
            tabBookingOversigt;
    public Label lblUser, lblUserName;
    public static SalgPane salgspane;
    private static RegningPane regningsPane;

    public static void main(String[] args)
    {
        Application.launch(args);
    }

    @Override
    public void init()
    {
        Service.getService().initStorage();
    }

    @Override
    public void start(Stage stage)
    {
        stage.setTitle("Aarhus Bryghus Kassesystem");
        BorderPane pane = new BorderPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }
    // -------------------------------------------------------------------------

    private void initContent(BorderPane pane)
    {
        TabPane tabPane = new TabPane();
        this.initTabPane(tabPane);
        pane.setCenter(tabPane);
        pane.setPrefSize(850, 700);
    }

    private void initTabPane(TabPane tabPane)
    {
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

        // ---------------------------------------------------------------

        this.tabSalgPane = new Tab("Salg");

        salgspane = new SalgPane(this);
        tabSalgPane.setContent(salgspane);

        // ---------------------------------------------------------------

        this.tabProdukter = new Tab("Produkter");

        ProdukterPane produktPane = new ProdukterPane();
        tabProdukter.setContent(produktPane);

        // ---------------------------------------------------------------
        this.tabPrisLister = new Tab("Prislister");

        PrislisterPane prislistePane = new PrislisterPane();
        tabPrisLister.setContent(prislistePane);
        // ---------------------------------------------------------------

        this.tabRegningPane = new Tab("Regning");

        MainApp.regningsPane = new RegningPane();
        tabRegningPane.setContent(regningsPane);

        // ---------------------------------------------------------------

        this.tabStatistik = new Tab("Statistik:");

        StatistikPane statistikPane = new StatistikPane();
        tabStatistik.setContent(statistikPane);

        // ---------------------------------------------------------------
        this.tabBookingOversigt = new Tab("Booking Oversigt");

        BookingOversigtPane bookingOversigt = new BookingOversigtPane();
        tabBookingOversigt.setContent(bookingOversigt);
        // ---------------------------------------------------------------
        // Inserting panes.
        tabPane.getTabs().add(tabSalgPane);
        tabPane.getTabs().add(tabProdukter);
        tabPane.getTabs().add(tabPrisLister);
        tabPane.getTabs().add(tabRegningPane);
        tabPane.getTabs().add(tabStatistik);
        tabPane.getTabs().add(tabBookingOversigt);
        // ---------------------------------------------------------------
    }

    public static SalgPane getSalgsPane()
    {
        return salgspane;
    }

    public static RegningPane getRegningsPane()
    {
        return regningsPane;
    }
}