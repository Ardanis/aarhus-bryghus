package gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import service.Service;

public class MobilePayModtagetWindow extends Stage
{

    public MobilePayModtagetWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    private Button btnOK, btnAnuller;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        btnOK = new Button("Pengene er modtaget");
        pane.add(btnOK, 0, 0);
        btnOK.setOnAction(event -> this.okAction());

        btnAnuller = new Button("Pengene er ikke modtaget");
        pane.add(btnAnuller, 0, 1);
        btnAnuller.setOnAction(event -> this.fortrydAction());
    }

    // -----------------------------------------------------
    // Button actions
    private void okAction()
    {
        Service.getService().afslutSalg();
        MainApp.getSalgsPane().clearAction();
        this.hide();
    }

    private void fortrydAction()
    {
        this.hide();
    }
}