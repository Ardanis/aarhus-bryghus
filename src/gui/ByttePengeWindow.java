package gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import service.Service;

public class ByttePengeWindow extends Stage
{
    public ByttePengeWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    private TextField txfByttepenge;
    private Button btnOK;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        txfByttepenge = new TextField("Byttepenge:");
        pane.add(txfByttepenge, 0, 0);
        txfByttepenge.setEditable(false);
        txfByttepenge.setText("" + Service.getService().getIgangvaerendeSalg().getByttepenge());

        btnOK = new Button("OK");
        pane.add(btnOK, 1, 0);
        btnOK.setOnAction(event -> this.okAction());
    }

    private void okAction()
    {
        Service.getService().afslutSalg();
        MainApp.getSalgsPane().clearAction();
        this.hide();
    }
}