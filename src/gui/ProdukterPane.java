package gui;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Produkt;
import model.Produktgruppe;
import service.Service;

public class ProdukterPane extends GridPane
{

    private ListView<Produktgruppe> lvwPGruppe;
    private ListView<Produkt> lvwProdukt;
    private TextField txfNavn;
    private TextArea txaBeskrivelse;

    private Label lbl1, lbl2, lbl3, lbl4;
    private Button btn1, btn2, btn3, btn4, btn5;

    public ProdukterPane()
    {
        this.setGridLinesVisible(false);
        this.setPadding(new Insets(30));
        this.setHgap(10);
        this.setVgap(10);

        this.setPrefSize(840, 456);
        lbl1 = new Label("Produktgrupper");
        this.add(lbl1, 1, 0);
        lbl2 = new Label("Produkter");
        this.add(lbl2, 2, 0);
        lbl3 = new Label("Navn");
        this.add(lbl3, 3, 0);
        lbl4 = new Label("Beskrivelse");
        this.add(lbl4, 3, 2);

        btn1 = new Button("Opret Produkt");
        this.add(btn1, 0, 1);
        btn2 = new Button("Opret Produktgruppe");
        this.add(btn2, 0, 2);
        btn3 = new Button("Rediger Produkt");
        this.add(btn3, 0, 3);
        btn4 = new Button("Rediger Produktgruppe");
        this.add(btn4, 0, 4);
        btn5 = new Button("Slet Produkt");
        this.add(btn5, 0, 5);
        GridPane.setValignment(btn5, VPos.TOP);

        lvwPGruppe = new ListView<>();
        lvwPGruppe.setMinSize(236, 350);
        lvwPGruppe.setMaxSize(236, 350);
        this.add(lvwPGruppe, 1, 1, 1, 7);
        for (int i = 0; i < Service.getService().getProduktgrupper().size(); i++) {
            lvwPGruppe.getItems().add(Service.getService().getProduktgrupper().get(i));
        }
        ChangeListener<Produktgruppe> listener = (ov, oldProduktgruppe, newProduktgruppe) -> this
                .opdaterProduktgruppeAction();
        lvwPGruppe.getSelectionModel().selectedItemProperty().addListener(listener);

        lvwProdukt = new ListView<>();
        lvwProdukt.setMinSize(195, 350);
        lvwProdukt.setMaxSize(195, 350);
        this.add(lvwProdukt, 2, 1, 1, 7);
        for (int i = 0; i < Service.getService().getProdukter().size(); i++) {
            lvwProdukt.getItems().add(Service.getService().getProdukter().get(i));
        }
        ChangeListener<Produkt> listener2 = (ov, oldProdukt, newProdukt) -> this
                .valgteProduktSkiftet();
        lvwProdukt.getSelectionModel().selectedItemProperty().addListener(listener2);

        txfNavn = new TextField();
        this.add(txfNavn, 3, 1);
        txfNavn.setEditable(false);
        txfNavn.setMaxSize(157, 25);
        txfNavn.setMinSize(157, 25);

        txaBeskrivelse = new TextArea();
        this.add(txaBeskrivelse, 3, 3, 1, 3);
        txaBeskrivelse.setEditable(false);
        txaBeskrivelse.setMinSize(157, 176);
        txaBeskrivelse.setMaxSize(157, 176);

        /*--------------------------------------------------------------------------------------------*/
        // Button Actions:

        btn1.setOnAction(event -> this.opretProduktAction());
        btn2.setOnAction(event -> this.opretGruppeAction());
        btn3.setOnAction(event -> this.redigerProduktAction());
        btn4.setOnAction(event -> this.redigerGruppeAction());
        btn5.setOnAction(event -> this.sletProduktAction());

    }

    private void opretProduktAction()
    {
        OpretProduktWindow win = new OpretProduktWindow("Opret Nyt Produkt");
        win.showAndWait();
        this.opdaterAction();
    }

    private void opretGruppeAction()
    {
        OpretProduktgruppeWindow win = new OpretProduktgruppeWindow("Opret Ny Produktgruppe");
        win.showAndWait();
        this.opdaterAction();
    }

    private void redigerProduktAction()
    {
        if (lvwProdukt.getSelectionModel().getSelectedItem() != null) {
            RedigerProduktWindow win = new RedigerProduktWindow("Rediger Produkt",
                    lvwProdukt.getSelectionModel().getSelectedItem());
            win.showAndWait();
        } else {
            System.out.println("Der er ikke valgt noget!");
        }
        this.opdaterAction();
    }

    private void redigerGruppeAction()
    {
        RedigerProduktgruppeWindow win = new RedigerProduktgruppeWindow("Rediger Produktgruppe",
                lvwPGruppe.getSelectionModel().getSelectedItem());
        win.showAndWait();
    }

    private void sletProduktAction()
    {
        Produkt tempProdukt = lvwProdukt.getSelectionModel().getSelectedItem();
        Service.getService().fjernProdukt(tempProdukt);
        this.opdaterAction();
    }

    private void valgteProduktSkiftet()
    {
        this.opdaterAction();
    }

    public void opdaterProduktgruppeAction()
    {
        txfNavn.clear();
        txaBeskrivelse.clear();

        Produktgruppe produktgruppe = lvwPGruppe.getSelectionModel().getSelectedItem();
        if (produktgruppe != null) {
            int index = lvwPGruppe.getSelectionModel().getSelectedIndex();
            Platform.runLater(() -> { // Kører denne bid til sidst.
                lvwProdukt.getItems()
                        .setAll(Service.getService().getProduktgrupper().get(index).getProdukter());
            });
        }

    }

    public void opdaterAction()
    {
        Produkt produkt = lvwProdukt.getSelectionModel().getSelectedItem();
        if (produkt != null) {
            int index = lvwProdukt.getSelectionModel().getSelectedIndex();
            txfNavn.setText(lvwProdukt.getItems().get(index).getNavn());
            txaBeskrivelse.setText(lvwProdukt.getItems().get(index).getBeskrivelse());
            for (int i = 0; i < produkt.getTilknyttedePriser().size(); i++) {
                System.out.println(produkt.getTilknyttedePriser().get(i).getPris());
            }
        } else {
            lvwProdukt.getItems().setAll(Service.getService().getProdukter());
            lvwPGruppe.getItems().setAll(Service.getService().getProduktgrupper());
        }
    }
}