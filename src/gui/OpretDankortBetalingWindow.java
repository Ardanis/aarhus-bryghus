package gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import service.Service;

public class OpretDankortBetalingWindow extends Stage
{

    public OpretDankortBetalingWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    private TextField txfPris, txfModtaget;
    private Button btnBetal, btnFortryd;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        txfPris = new TextField("Resterende Beløb: "
                + Service.getService().getIgangvaerendeSalg().getManglerAtBetale());
        txfPris.setEditable(false);
        pane.add(txfPris, 0, 0);

        txfModtaget = new TextField("Modtaget");
        pane.add(txfModtaget, 0, 1);

        btnBetal = new Button("Betal");
        pane.add(btnBetal, 1, 0);
        btnBetal.setOnAction(event -> this.betalAction());

        btnFortryd = new Button("Annuller");
        pane.add(btnFortryd, 1, 1);
        btnFortryd.setOnAction(event -> this.closeWindowAction());
    }

    private void betalAction()
    {
        if (Integer.parseInt(txfModtaget.getText()) >= Service.getService().getIgangvaerendeSalg()
                .getManglerAtBetale()) {
            Service.getService().getIgangvaerendeSalg()
                    .setByttepenge((Service.getService().getIgangvaerendeSalg().getManglerAtBetale()
                            - Integer.parseInt(txfModtaget.getText())) * -1);
            ByttePengeWindow win = new ByttePengeWindow("Penge Retur");
            win.showAndWait();
            this.hide();
        }
    }

    private void closeWindowAction()
    {
        this.hide();
    }
}