package gui;

import java.time.LocalDate;
import java.util.ArrayList;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Antal;
import model.Klip;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import model.Produktgruppe;
import service.Service;

public class SalgPane extends GridPane
{
    private int[] pos = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
    private Produktgruppe selectedProduktgruppe;
    private Prisliste selectedPrisliste;
    private ArrayList<Button> buttonArray = new ArrayList<>();
    private ArrayList<Button> prevNextButton = new ArrayList<>();
    private ArrayList<TextField> textFieldArray = new ArrayList<>();
    private ArrayList<Pris> activeArray = new ArrayList<>();
    private ListView<Antal> list;
    private ArrayList<Produkt> tempProdukter;

    private MainApp mainApp;

    public SalgPane(MainApp mainApp)
    {
        this.mainApp = mainApp;

        this.setGridLinesVisible(false);
        // set padding of the pane
        this.setPadding(new Insets(30));
        // set horizontal gap between components
        this.setHgap(10);
        // set vertical gap between components
        this.setVgap(10);
        // this.setPrefSize(1100, 800);

        ComboBox<Prisliste> CBprisListe = new ComboBox<>();
        this.add(CBprisListe, 0, 0, 2, 1);
        CBprisListe.setMinWidth(109);
        for (Prisliste pListe : Service.getService().getPrislister()) {
            CBprisListe.getItems().add(pListe);
        }
        CBprisListe.setOnAction(event -> {
            selectedPrisliste = CBprisListe.getSelectionModel().getSelectedItem();
            this.buttonUpdateProduktGruppe(selectedProduktgruppe, selectedPrisliste);
        });

        ComboBox<Produktgruppe> CBProduktgruppe = new ComboBox<>();
        this.add(CBProduktgruppe, 2, 0, 2, 1);
        CBProduktgruppe.setMinWidth(109);
        for (Produktgruppe pGruppe : Service.getService().getProduktgrupper()) {
            CBProduktgruppe.getItems().add(pGruppe);
        }
        CBProduktgruppe.setOnAction(event -> {
            selectedProduktgruppe = CBProduktgruppe.getSelectionModel().getSelectedItem();
            this.buttonUpdateProduktGruppe(selectedProduktgruppe, selectedPrisliste);
        });

        Button btn1 = new Button(
                this.buttonName2(pos[0], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn1, 0, 1);
        btn1.setMinSize(109, 52);
        buttonArray.add(btn1);
        btn1.setOnAction(event -> this.buttonAction(pos[0],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn2 = new Button(
                this.buttonName2(pos[1], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn2, 1, 1);
        btn2.setMinSize(109, 52);
        buttonArray.add(btn2);
        btn2.setOnAction(event -> this.buttonAction(pos[1],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn3 = new Button(
                this.buttonName2(pos[2], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn3, 2, 1);
        btn3.setMinSize(109, 52);
        buttonArray.add(btn3);
        btn3.setOnAction(event -> this.buttonAction(pos[2],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn4 = new Button(
                this.buttonName2(pos[3], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn4, 0, 2);
        btn4.setMinSize(109, 52);
        buttonArray.add(btn4);
        btn4.setOnAction(event -> this.buttonAction(pos[3],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn5 = new Button(
                this.buttonName2(pos[4], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn5, 1, 2);
        btn5.setMinSize(109, 52);
        buttonArray.add(btn5);
        btn5.setOnAction(event -> this.buttonAction(pos[4],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn6 = new Button(
                this.buttonName2(pos[5], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn6, 2, 2);
        btn6.setMinSize(109, 52);
        buttonArray.add(btn6);
        btn6.setOnAction(event -> this.buttonAction(pos[5],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn7 = new Button(
                this.buttonName2(pos[6], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn7, 0, 3);
        btn7.setMinSize(109, 52);
        buttonArray.add(btn7);
        btn7.setOnAction(event -> this.buttonAction(pos[6],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn8 = new Button(
                this.buttonName2(pos[7], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn8, 1, 3);
        btn8.setMinSize(109, 52);
        buttonArray.add(btn8);
        btn8.setOnAction(event -> this.buttonAction(pos[7],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn9 = new Button(
                this.buttonName2(pos[8], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn9, 2, 3);
        btn9.setMinSize(109, 52);
        buttonArray.add(btn9);
        btn9.setOnAction(event -> this.buttonAction(pos[8],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn10 = new Button(
                this.buttonName2(pos[9], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn10, 0, 4);
        btn10.setMinSize(109, 52);
        buttonArray.add(btn10);
        btn10.setOnAction(event -> this.buttonAction(pos[9],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn11 = new Button(
                this.buttonName2(pos[10], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn11, 1, 4);
        btn11.setMinSize(109, 52);
        buttonArray.add(btn11);
        btn11.setOnAction(event -> this.buttonAction(pos[10],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn12 = new Button(
                this.buttonName2(pos[11], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn12, 2, 4);
        btn12.setMinSize(109, 52);
        buttonArray.add(btn12);
        btn12.setOnAction(event -> this.buttonAction(pos[11],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn13 = new Button(
                this.buttonName2(pos[12], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn13, 0, 5);
        btn13.setMinSize(109, 52);
        buttonArray.add(btn13);
        btn13.setOnAction(event -> this.buttonAction(pos[12],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn14 = new Button(
                this.buttonName2(pos[13], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn14, 1, 5);
        btn14.setMinSize(109, 52);
        buttonArray.add(btn14);
        btn14.setOnAction(event -> this.buttonAction(pos[13],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn15 = new Button(
                this.buttonName2(pos[14], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn15, 2, 5);
        btn15.setMinSize(109, 52);
        buttonArray.add(btn15);
        btn15.setOnAction(event -> this.buttonAction(pos[14],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn16 = new Button(
                this.buttonName2(pos[15], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn16, 0, 6);
        btn16.setMinSize(109, 52);
        buttonArray.add(btn16);
        btn16.setOnAction(event -> this.buttonAction(pos[15],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn17 = new Button(
                this.buttonName2(pos[16], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn17, 1, 6);
        btn17.setMinSize(109, 52);
        buttonArray.add(btn17);
        btn17.setOnAction(event -> this.buttonAction(pos[16],
                Integer.parseInt(textFieldArray.get(0).getText())));

        Button btn18 = new Button(
                this.buttonName2(pos[17], CBProduktgruppe.getSelectionModel().getSelectedItem(),
                        CBprisListe.getSelectionModel().getSelectedItem()));
        this.add(btn18, 2, 6);
        btn18.setMinSize(109, 52);
        buttonArray.add(btn18);
        btn18.setOnAction(event -> this.buttonAction(pos[17],
                Integer.parseInt(textFieldArray.get(0).getText())));

        for (Button b : buttonArray) {
            b.setDisable(true);
        }

        Button btnPrev = new Button("<");
        this.add(btnPrev, 1, 7);
        GridPane.setHalignment(btnPrev, HPos.LEFT);
        prevNextButton.add(btnPrev);
        btnPrev.setDisable(true);
        btnPrev.setOnAction(event -> this.actionPrev());

        Button btnNext = new Button(">");
        this.add(btnNext, 1, 7);
        GridPane.setHalignment(btnNext, HPos.RIGHT);
        prevNextButton.add(btnNext);
        btnNext.setDisable(true);
        btnNext.setOnAction(event -> this.actionNext());

        Label lblValgteProdukter = new Label("Valgte Produkter");
        this.add(lblValgteProdukter, 3, 0, 4, 1);

        list = new ListView<>();
        list.setMaxHeight(300);
        // lvwSalgProdukter.setMinSize(150, 150);
        // lvwSalgProdukter.setMaxSize(150, 150);
        this.add(list, 3, 1, 4, 5);

        TextField txfPris = new TextField("0");
        this.add(txfPris, 3, 6, 4, 2);
        txfPris.setMinWidth(100);
        txfPris.setEditable(false);
        textFieldArray.add(txfPris);

        TextField txfSamlet = new TextField("Samlet Pris: ");
        this.add(txfSamlet, 3, 6, 4, 1);
        txfSamlet.setMinWidth(100);
        textFieldArray.add(txfSamlet);
        txfSamlet.setEditable(false);
        GridPane.setValignment(txfSamlet, VPos.TOP);

        Button btnSlet = new Button("Slet");
        this.add(btnSlet, 3, 7, 3, 1);
        btnSlet.setMinSize(88, 25);
        btnSlet.setOnAction(event -> this.sletAction(list.getSelectionModel().getSelectedItem()));

        Button btnBetal = new Button("Betal");
        this.add(btnBetal, 5, 7, 3, 1);
        btnBetal.setMinSize(88, 25);
        btnBetal.setOnAction(event -> this.betalAction());

        Button btnKlip = new Button("Klip");
        this.add(btnKlip, 3, 10);
        btnKlip.setMinSize(39, 25);
        btnKlip.setOnAction(event -> this.klipAction(LocalDate.now(),
                list.getSelectionModel().getSelectedItem().getPris()));

        Button btnUdlejning = new Button("Udlejning");
        this.add(btnUdlejning, 3, 12, 2, 1);
        btnUdlejning.setOnAction(event -> this.udlejAction());

        Button btnRundvisning = new Button("Rundvisning");
        this.add(btnRundvisning, 5, 12, 2, 1);
        btnRundvisning.setOnAction(event -> this.rundvisningAction());

        Button btnFast = new Button("Fast");
        this.add(btnFast, 3, 8);
        btnFast.setMinSize(39, 25);
        btnFast.setOnAction(
                event -> this.fastRabatAction(Integer.parseInt(textFieldArray.get(0).getText())));

        Button btnProcent = new Button("%");
        this.add(btnProcent, 3, 9);
        btnProcent.setMinSize(39, 25);
        btnProcent.setOnAction(
                event -> this.pctRabatAction(Integer.parseInt(textFieldArray.get(0).getText())));

        Button btnAntal7 = new Button("7");
        this.add(btnAntal7, 4, 8);
        btnAntal7.setMinSize(39, 25);
        btnAntal7.setOnAction(event -> this.numberAction(7));

        Button btnAntal8 = new Button("8");
        this.add(btnAntal8, 5, 8);
        btnAntal8.setMinSize(39, 25);
        btnAntal8.setOnAction(event -> this.numberAction(8));

        Button btnAntal9 = new Button("9");
        this.add(btnAntal9, 6, 8);
        btnAntal9.setMinSize(39, 25);
        btnAntal9.setOnAction(event -> this.numberAction(9));

        Button btnAntal4 = new Button("4");
        this.add(btnAntal4, 4, 9);
        btnAntal4.setMinSize(39, 25);
        btnAntal4.setOnAction(event -> this.numberAction(4));

        Button btnAntal5 = new Button("5");
        this.add(btnAntal5, 5, 9);
        btnAntal5.setMinSize(39, 25);
        btnAntal5.setOnAction(event -> this.numberAction(5));

        Button btnAntal6 = new Button("6");
        this.add(btnAntal6, 6, 9);
        btnAntal6.setMinSize(39, 25);
        btnAntal6.setOnAction(event -> this.numberAction(6));

        Button btnAntal1 = new Button("1");
        this.add(btnAntal1, 4, 10);
        btnAntal1.setMinSize(39, 25);
        btnAntal1.setOnAction(event -> this.numberAction(1));

        Button btnAntal2 = new Button("2");
        this.add(btnAntal2, 5, 10);
        btnAntal2.setMinSize(39, 25);
        btnAntal2.setOnAction(event -> this.numberAction(2));

        Button btnAntal3 = new Button("3");
        this.add(btnAntal3, 6, 10);
        btnAntal3.setMinSize(39, 25);
        btnAntal3.setOnAction(event -> this.numberAction(3));

        Button btnAntalC = new Button("C");
        this.add(btnAntalC, 4, 11);
        btnAntalC.setMinSize(39, 25);
        btnAntalC.setOnAction(event -> txfPris.setText("0"));

        Button btnAntal0 = new Button("0");
        this.add(btnAntal0, 5, 11);
        btnAntal0.setMinSize(39, 25);
        btnAntal0.setOnAction(event -> this.numberAction(0));

        Button btnAntalX = new Button("X");
        this.add(btnAntalX, 6, 11);
        btnAntalX.setMinSize(39, 25);

    }

    private void rundvisningAction()
    {
        OpretRundvisningWindow win = new OpretRundvisningWindow("Opret Rundvisning");
        win.showAndWait();
    }

    private void udlejAction()
    {
        Service.getService().udlej();
        MainApp.getSalgsPane().clearAction();
    }

    private void betalAction()
    {
        VælgBetalingsFormWindow win = new VælgBetalingsFormWindow("Vælg Betalingsform");
        Service.getService().getIgangvaerendeSalg()
                .setManglerAtBetale(Service.getService().getIgangvaerendeSalg().getSamletPris());
        win.showAndWait();
    }

    private void buttonUpdateProduktGruppe(Produktgruppe pGruppe, Prisliste pListe)
    {
        tempProdukter = new ArrayList<>();
        int i = pos[0];
        for (Button b : buttonArray) {
            b.setText(this.buttonName2(i, pGruppe, pListe));
            i++;
            if (b.getText().equals("Tom")) {
                b.setDisable(true);
            } else {
                b.setDisable(false);
            }
        }
        if (tempProdukter.size() - 2 < pos[17]) {
            prevNextButton.get(1).setDisable(true);
        } else {
            prevNextButton.get(1).setDisable(false);
        }

        if (pos[0] == 0) {
            prevNextButton.get(0).setDisable(true);
        } else {
            prevNextButton.get(0).setDisable(false);
        }
    }

    private void buttonAction(int pos, int antal)
    {
        Antal tempAntal = new Antal(antal, activeArray.get(pos));
        if (!textFieldArray.get(0).getText().equals("0")) {
            if (Service.getService().getIgangvaerendeSalg() == null) {
                Service.getService().startSalg(tempAntal);
            } else {
                Service.getService().tilføjTilSalg(tempAntal);
            }
            list.getItems().add(tempAntal);
            textFieldArray.get(1).setText("Samlet Pris: "
                    + Service.getService().getIgangvaerendeSalg().getSamletPris() + " kr");
            textFieldArray.get(0).setText("0");
        }
    }

    private void sletAction(Antal antal)
    {
        Service.getService().fjernAntal(antal);
        list.getItems().remove(antal);
        textFieldArray.get(1).setText("Samlet Pris: "
                + Service.getService().getIgangvaerendeSalg().getSamletPris() + " kr");
    }

    private void actionNext()
    {
        int i = 0;
        for (int posInt : pos) {
            pos[i] += 18;
        }
        this.buttonUpdateProduktGruppe(selectedProduktgruppe, selectedPrisliste);
    }

    private void actionPrev()
    {
        int i = 0;
        for (int posInt : pos) {
            pos[i] -= 18;
        }
        this.buttonUpdateProduktGruppe(selectedProduktgruppe, selectedPrisliste);
    }

    private String buttonName2(int i, Produktgruppe pGruppe, Prisliste pListe)
    {
        tempProdukter = new ArrayList<>();
        activeArray = new ArrayList<>();
        if (pGruppe != null && pListe != null) {
            for (Pris pris : pListe.getPriser()) {
                for (Produkt produkt : pGruppe.getProdukter()) {
                    if (produkt.getNavn() == pris.getProdukt().getNavn()) {
                        if (tempProdukter.contains(produkt) == false) {
                            tempProdukter.add(produkt);
                            activeArray.add(pris);
                        }
                    }
                }
            }
        }

        if (tempProdukter.size() - 1 < i) {
            return "Tom";
        } else if (pListe != null && pGruppe != null) {
            return tempProdukter.get(i).getNavn();
        } else {
            return "Tom";
        }
    }

    private void fastRabatAction(double i)
    {
        if (Service.getService().getIgangvaerendeSalg().getSamletPris() - i >= 0) {
            Service.getService().getIgangvaerendeSalg()
                    .setSamletPris(Service.getService().getIgangvaerendeSalg().getSamletPris() - i);
            textFieldArray.get(1).setText("Samlet Pris: "
                    + Service.getService().getIgangvaerendeSalg().getSamletPris() + " kr");
        }
    }

    private void pctRabatAction(double i)
    {
        if (i < 100) {
            Service.getService().getIgangvaerendeSalg()
                    .setSamletPris(Service.getService().getIgangvaerendeSalg().getSamletPris()
                            * ((100 - i) / 100));
            textFieldArray.get(1).setText("Samlet Pris: "
                    + Service.getService().getIgangvaerendeSalg().getSamletPris() + " kr");
        }
    }

    private void numberAction(int i)
    {
        if (textFieldArray.get(0).getText().equals("Pris")
                || textFieldArray.get(0).getText().equals("0")) {
            textFieldArray.get(0).setText("" + i);
        } else {
            textFieldArray.get(0).setText("" + textFieldArray.get(0).getText() + i);
        }

    }

    public void clearAction()
    {
        textFieldArray.get(0).setText("0");
        textFieldArray.get(1).setText("Samlet Pris: ");
        list.getItems().clear();
    }

    private void klipAction(LocalDate dato, Pris pris)
    {
        if (pris != null && pris.getPris() < 51) {
            Klip klip = new Klip(dato, pris.getProdukt());
            Service.getService().getIgangvaerendeSalg().setSamletPris(
                    Service.getService().getIgangvaerendeSalg().getSamletPris() - pris.getPris());
            Service.getService().getAlleKlip().add(klip);
            textFieldArray.get(1).setText("Samlet Pris: "
                    + Service.getService().getIgangvaerendeSalg().getSamletPris() + " kr");
            textFieldArray.get(0).setText("0");
        }
    }
