package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class VælgBetalingsFormWindow extends Stage
{

    public VælgBetalingsFormWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -------------------------------------------------------------------------

    private Button btn1, btn2, btn3, btn4, btnAnuller;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        btn1 = new Button("Dankort");
        pane.add(btn1, 0, 0);
        btn1.setOnAction(event -> this.openDankortBetalingAction());

        btn2 = new Button("MobilePay");
        pane.add(btn2, 1, 0);
        btn2.setOnAction(event -> this.openMobilePayBetalingAction());

        btn3 = new Button("Kontant");
        pane.add(btn3, 2, 0);
        btn3.setOnAction(event -> this.openKontantBetalingAction());

        btn4 = new Button("Regning");
        pane.add(btn4, 3, 0);
        btn4.setOnAction(event -> this.openRegningBetalingAction());

        btnAnuller = new Button("Fortryd");
        pane.add(btnAnuller, 1, 1, 2, 1);
        btnAnuller.setOnAction(event -> this.cancelAction());
        GridPane.setHalignment(btnAnuller, HPos.CENTER);
    }

    // -----------------------------------------------------
    // Button actions
    private void openDankortBetalingAction()
    {
        OpretDankortBetalingWindow win = new OpretDankortBetalingWindow("Betal med Dankort");
        win.showAndWait();
        this.hide();
    }

    private void openMobilePayBetalingAction()
    {
        OpretMobilePayBetalingWindow win = new OpretMobilePayBetalingWindow("Betal med MobilePay");
        win.showAndWait();
        this.hide();
    }

    private void openKontantBetalingAction()
    {
        OpretKontantBetalingWindow win = new OpretKontantBetalingWindow("Betal med Kontanter");
        win.showAndWait();
        this.hide();
    }

    private void openRegningBetalingAction()
    {
        OpretRegningBetalingWindow win = new OpretRegningBetalingWindow("Betal med Regning");
        win.showAndWait();
        this.hide();
    }

    private void cancelAction()
    {
        this.hide();
    }
}