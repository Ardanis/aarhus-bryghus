package gui;

import java.util.ArrayList;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Klip;
import model.Produkt;
import model.Salg;
import service.Service;

public class StatistikPane extends GridPane
{

    private ListView<Salg> lvwSalg;
    private ListView<Produkt> lvwProdukt;
    private TextField txfKlip;
    private ListView<Produkt> lvwProduktKøbtMedKlip;
    private DatePicker dpDato;

    private Label lbl1, lbl2;

    public StatistikPane()
    {
        this.setGridLinesVisible(true);
        this.setPadding(new Insets(30));
        this.setHgap(10);
        this.setVgap(10);

        this.setPrefSize(840, 456);
        lbl1 = new Label("Salg på valgte dato");
        this.add(lbl1, 1, 0);
        lbl2 = new Label("Solgte Produkter");
        this.add(lbl2, 2, 0);

        dpDato = new DatePicker();
        this.add(dpDato, 0, 0);
        dpDato.setOnAction(event -> {
            lvwProdukt.getItems().clear();
            lvwSalg.getItems().clear();
            this.opdaterAction();
        });

        lvwSalg = new ListView<>();
        lvwSalg.setMinSize(236, 350);
        lvwSalg.setMaxSize(236, 350);
        this.add(lvwSalg, 1, 1, 1, 7);
        ChangeListener<Salg> listener = (ov, oldRegning, newRegning) -> this
                .opdaterProdukterAction();
        lvwSalg.getSelectionModel().selectedItemProperty().addListener(listener);

        lvwProdukt = new ListView<>();
        lvwProdukt.setMinSize(195, 350);
        lvwProdukt.setMaxSize(195, 350);
        this.add(lvwProdukt, 2, 1, 1, 7);

        txfKlip = new TextField("Antal klip på dagen");
        this.add(txfKlip, 1, 8);

        lvwProduktKøbtMedKlip = new ListView<>();
        lvwProduktKøbtMedKlip.setMinSize(195, 350);
        lvwProduktKøbtMedKlip.setMaxSize(195, 350);
        this.add(lvwProduktKøbtMedKlip, 2, 8, 1, 7);
    }

    /*--------------------------------------------------------------------------------------------*/
    private void opdaterAction()
    {
        for (int i = 0; i < Service.getService().getSalg().size(); i++) {
            if (Service.getService().getSalg().get(i).getSalgsDato().equals(dpDato.getValue())) {
                lvwSalg.getItems().setAll(Service.getService().getSalg());
                ArrayList<Klip> tempKlip = new ArrayList<>();
                for (int j = 0; j < Service.getService().getAlleKlip().size(); j++) {
                    if (Service.getService().getAlleKlip().get(j).getDato()
                            .equals(dpDato.getValue())) {
                        tempKlip.add(Service.getService().getAlleKlip().get(j));
                        ArrayList<Produkt> tempProdukt = new ArrayList<>();
                        tempProdukt.add(Service.getService().getAlleKlip().get(j).getProdukt());
                        lvwProduktKøbtMedKlip.getItems().setAll(tempProdukt);
                    }
                }
                txfKlip.setText(tempKlip.size() + "");
            }
        }
    }

    private void opdaterProdukterAction()
    {
        Salg salg = lvwSalg.getSelectionModel().getSelectedItem();
        if (salg != null) {
            for (int i = 0; i < salg.getPriser().size(); i++) {
                lvwProdukt.getItems().add((salg.getPriser().get(i).getPris().getProdukt()));
            }
        }
    }
}