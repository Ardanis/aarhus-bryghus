package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Prisliste;
import model.Produkt;
import service.Service;

public class RedigerPrislisteWindow extends Stage
{
    private Prisliste valgtePrisliste;

    public RedigerPrislisteWindow(String title, Prisliste valgtePrisliste)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);
        this.valgtePrisliste = valgtePrisliste;

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -------------------------------------------------------------------------

    private TextField txfNavn, txfPris;
    private ListView<Produkt> lvwAlleProdukter, lvwTilføjedeProdukter;
    private Label lblAlleProdukter, lblTilføjedeProdukter, lblError;
    private Button btnGem, btnAnnuller, btnTilføj, btnFjern;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        lblAlleProdukter = new Label("Alle Produkter:");
        pane.add(lblAlleProdukter, 0, 1);

        lblTilføjedeProdukter = new Label("Tilføjede Produkter");
        pane.add(lblTilføjedeProdukter, 2, 1);

        lblError = new Label();
        lblError.setStyle("-fx-text-fill: red");
        pane.add(lblError, 0, 6);
        GridPane.setHalignment(lblError, HPos.LEFT);

        txfNavn = new TextField("Navn");
        pane.add(txfNavn, 0, 0);
        txfNavn.setText(valgtePrisliste.getNavn());

        txfPris = new TextField("Pris");
        pane.add(txfPris, 2, 0);

        lvwAlleProdukter = new ListView<>();
        pane.add(lvwAlleProdukter, 0, 2, 1, 4);
        for (int i = 0; i < Service.getService().getProdukter().size(); i++) {
            lvwAlleProdukter.getItems().add(Service.getService().getProdukter().get(i));
        }

        lvwTilføjedeProdukter = new ListView<>();
        pane.add(lvwTilføjedeProdukter, 2, 2, 1, 4);
        lvwTilføjedeProdukter.getItems().addAll(valgtePrisliste.getProdukter());
        ChangeListener<Produkt> listener = (ov, oldProdukt, newProdukt) -> this
                .valgteProduktSkiftet();
        lvwTilføjedeProdukter.getSelectionModel().selectedItemProperty().addListener(listener);

        btnTilføj = new Button(">");
        pane.add(btnTilføj, 1, 4);
        btnTilføj.setOnAction(event -> this.tilføjAction());

        btnFjern = new Button("<");
        pane.add(btnFjern, 1, 5);
        GridPane.setValignment(btnFjern, VPos.TOP);
        btnFjern.setOnAction(event -> this.fjernAction());

        btnGem = new Button("Gem");
        pane.add(btnGem, 0, 6);
        GridPane.setHalignment(btnGem, HPos.RIGHT);
        btnGem.setOnAction(event -> this.gemAction());

        btnAnnuller = new Button("Fortryd");
        pane.add(btnAnnuller, 2, 6);
        btnAnnuller.setOnAction(event -> this.lukWindowAction());
    }

    // -----------------------------------------------------
    // Button actions

    private void valgteProduktSkiftet()
    {
        this.opdater();
    }

    private void lukWindowAction()
    {
        this.hide();
    }

    private void gemAction()
    {
        boolean eksisterer = false;
        for (int i = 0; i < Service.getService().getPrislister().size(); i++) {
            if (Service.getService().getPrislister().get(i).getNavn().equals(txfNavn.getText())) {
                eksisterer = true;
            }

            if (txfNavn.getText().equals(valgtePrisliste.getNavn())) {
                this.hide();
            } else if (!eksisterer) {
                valgtePrisliste.setNavn(txfNavn.getText());
                this.hide();
            } else {
                lblError.setText("Prisliste eksisterer allerede");
            }
        }

    }

    private void fjernAction()
    {
        Service.getService().fjernPrisFraPrisliste(valgtePrisliste, valgtePrisliste.getPriser()
                .get(lvwTilføjedeProdukter.getSelectionModel().getSelectedIndex()));
        this.opdater();
    }

    private void tilføjAction()
    {
        if (txfPris.getText().equals("Pris") || txfPris.getText().isEmpty()) {
            lblError.setText("Der er ikke sat en pris på varen!");
        } else {
            Service.getService().opretPris(Double.parseDouble(txfPris.getText()),
                    lvwAlleProdukter.getSelectionModel().getSelectedItem(), valgtePrisliste);
            lblError.setText("");
            this.opdater();
        }
    }

    private void opdater()
    {
        Produkt produkt = lvwTilføjedeProdukter.getSelectionModel().getSelectedItem();
        if (produkt != null) {
            txfPris.setText(Double.toString(valgtePrisliste.getPriser()
                    .get(lvwTilføjedeProdukter.getSelectionModel().getSelectedIndex()).getPris()));
        } else {
            txfPris.clear();
        }
        lvwTilføjedeProdukter.getItems().setAll(valgtePrisliste.getProdukter());
    }
}