package gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Regning;
import service.Service;

public class OpretRegningBetalingWindow extends Stage
{
    public OpretRegningBetalingWindow(String title)
    {
        this.initStyle(StageStyle.UTILITY);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle(title);
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    private ListView<Regning> lvwRegninger;
    private TextField txfPris, txfNyRegning;
    private Button btnBetal, btnFortryd, btnOpretRegning;

    private void initContent(GridPane pane)
    {
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setGridLinesVisible(false);

        lvwRegninger = new ListView<>();
        for (Regning r : Service.getService().getRegninger()) {
            lvwRegninger.getItems().add(r);
        }

        pane.add(lvwRegninger, 0, 1);

        txfPris = new TextField(
                "Samlet Beløb " + Service.getService().getIgangvaerendeSalg().getSamletPris());
        pane.add(txfPris, 0, 0);

        btnBetal = new Button("Tilføj til Regning");
        pane.add(btnBetal, 1, 0);
        btnBetal.setOnAction(
                event -> this.betalAction(lvwRegninger.getSelectionModel().getSelectedItem()));

        btnFortryd = new Button("Annuller");
        pane.add(btnFortryd, 1, 1);
        btnFortryd.setOnAction(event -> this.closeWindowAction());

        txfNyRegning = new TextField("Opret en regning");
        pane.add(txfNyRegning, 1, 2);

        btnOpretRegning = new Button("Opret");
        pane.add(btnOpretRegning, 1, 3);
        btnOpretRegning.setOnAction(event -> this.opretNyRegning());

    }

    // -----------------------------------------------------
    // Button actions
    private void betalAction(Regning regning)
    {
        regning.tilføjSalg(Service.getService().getIgangvaerendeSalg());
        Service.getService().afslutSalg();
        MainApp.getSalgsPane().clearAction();
        MainApp.getRegningsPane().opdaterAlt();
        this.hide();
    }

    private void opretNyRegning()
    {
        if (!txfNyRegning.getText().equals("Opret en regning")) {
            Regning tempRegning = Service.getService().opretRegning(txfNyRegning.getText());
            tempRegning.tilføjSalg(Service.getService().getIgangvaerendeSalg());
            lvwRegninger.getItems().add(tempRegning);
            Service.getService().afslutSalg();
            MainApp.getSalgsPane().clearAction();
            MainApp.getRegningsPane().opdaterAlt();
            this.hide();
        }
    }

    private void closeWindowAction()
    {
        this.hide();
    }
}